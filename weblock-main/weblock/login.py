# done ----------------------------------------

import _tkinter
from tkinter import *
from tkinter import messagebox
from tkinter.font import Font
from static_functions import *
from main_menu import MainMenu
from dns_server import start_dns_server


GREY = "#878C9F"
BLUE = "#4966C7"
WHITE = "white"
FONT = "Helvetica 20"
RETURN = "Return"
ENTRY_RELIEF = RAISED


class Login(Frame):
    def __init__(self, master, last_frame, sock):
        """
        the constructor function of the class
        :param master: the master of the class (the App class object)
        :param last_frame: the frame that was on the screen before the current frame
        """
        Frame.__init__(self, master)
        self.master = master
        self.last_frame = last_frame
        self.sock = sock
        self.return_button = Button(self, text=RETURN, font=FONT, bg="green",
                                    command=lambda: [master.switch_frame(last_frame.last_frame, last_frame.__class__),
                                                     close_socket(self.sock)])
        self.canvas = Canvas(self, bg=GREY)
        self.username = Label(self.canvas, text="Username:", bg=GREY, font=Font(size=14))
        self.enter_username = Entry(self.canvas, font=Font(size=16), relief=ENTRY_RELIEF)

        self.password = Label(self.canvas, text="Password:", bg=GREY, font=Font(size=14))
        self.enter_password = Entry(self.canvas, font=Font(size=16), show="*", relief=ENTRY_RELIEF)

        self.login_button = Button(self.canvas, text="Log In", bg=BLUE, fg=WHITE, font=Font(size=16),
                                   command=lambda: self.login())

        self.master.bind(ENTER, lambda event=None: self.login_button.invoke())
        self.place_widgets()

    def place_widgets(self):
        self.return_button.place(relx=0.88, rely=0.85)
        self.canvas.place(relx=0.5, y=30, width=500, height=400, anchor='n')
        self.username.place(x=100, y=35)
        self.enter_username.place(x=100, y=65, width=300, height=45)
        self.password.place(x=100, y=130)
        self.enter_password.place(x=100, y=160, width=300, height=45)
        self.login_button.place(relx=0.5, rely=0.85, width=120, anchor=CENTER)

    def login(self):
        """
        try to login with the entered details
        :return: None
        """
        if self.sock:
            name = self.enter_username.get()  # get the name that the user entered
            password = self.enter_password.get()  # get the password that the user entered
            name = check_valid_name(name)  # check if the name is in the valid form
            if name:
                if valid_password_form(password):  # check if the password is in the valid form
                    send_user_details(self.sock, LOGIN, name, password)  # send the details to the admin server
                    response = self.sock.recv(1024).decode()  # get response from the admin server
                    self.handle_response(response)  # handle the response

    def handle_response(self, response):
        """
        if the username and password is correct moves to the frame of the network,
         otherwise show message that says incorrect details
        :param response: the response of the Admin server for the login request
        :return: None
        """
        if response == VALID:
            close_socket(self.sock)
            self.master.switch_frame(self, MainMenu)
            self.master.set_blocked_webs()
            self.master.set_not_shown()
            self.master.start_socket()
            start_dns_server(self.master)
        else:
            messagebox.showwarning(INVALID, "Incorrect username or password")
