# done ----------------------------------------

import _tkinter
from tkinter import *
from tkinter import messagebox
from tkinter.font import Font
from static_functions import *


HEADER_FONT = "Algerian 24 bold"
HEADER_COLOR = "dark slate gray"
HEADER_X_POS = 0.5
HEADER_Y_POS = 6
GREY = "#878C9F"
BLUE = "#4966C7"
WHITE = "white"
DARK_BLUE = "#152A7A"
BLACK = "black"
FONT = "Helvetica 20"
RETURN = "Return"
CANCEL = "Cancel"


class ClientStats(Frame):
    def __init__(self, master, last_frame, client_name):
        """
        the constructor function of the class
        :param master: the master of the class (the App class object)
        :param last_frame: the frame that was on the screen before the current frame
        :param client_name: the name of the client that the it is his statistics
        """
        Frame.__init__(self, master)
        self.master = master
        self.last_frame = last_frame
        self.pop = None
        self.client_name = client_name

        self.header = Label(self, text=f"{self.client_name}'s Statistics", font=HEADER_FONT, fg=HEADER_COLOR)
        self.return_button = Button(self, text=RETURN, font=FONT, bg="green",
                                    command=lambda: master.switch_frame(last_frame.last_frame, last_frame.__class__))

        self.date_list = Listbox(self, bg=GREY, fg=WHITE, font=Font(size=16), activestyle='none', borderwidth=0,
                                 selectmode=SINGLE, exportselection=False, highlightthickness=0)

        self.time_list = Listbox(self, bg=GREY, fg=WHITE, font=Font(size=16), activestyle='none', borderwidth=0,
                                 selectmode=SINGLE, exportselection=False, highlightthickness=0)

        self.websites_label = Label(self, text="Websites:", fg=BLUE, font=Font(size=18, weight="bold"))

        self.websites_list = Listbox(self, bg=GREY, fg=WHITE, font=Font(size=16), activestyle='none', borderwidth=0,
                                     selectmode=SINGLE, exportselection=False, highlightthickness=0)

        self.status_label = Label(self, text="Status:", fg=BLUE,
                                  font=Font(size=18, weight="bold"))

        self.status_list = Listbox(self, bg=GREY, fg=WHITE, font=Font(size=16), activestyle='none',
                                   selectmode=SINGLE, exportselection=False, highlightthickness=0)

        self.listboxes = [self.date_list, self.time_list, self.websites_list, self.status_list]

        self.scrollbar = Scrollbar(self.status_list, orient=VERTICAL,
                                   command=lambda e: on_vsb(e, self.listboxes))

        self.clear_stats = Button(self, text="Clear\nStats", bg=DARK_BLUE, fg=WHITE, font=Font(size=17),
                                  command=lambda: confirm_clear(self.master, self.listboxes,
                                                                CLIENTS_STATS, self.client_name))

        self.refresh = Button(self, text="Refresh", bg=BLUE, fg=WHITE, font=Font(size=17),
                              command=lambda: refresh_stats(self.listboxes, self.client_name))

        self.bind_listboxes_to_press()
        self.place_widgets()
        bind_listboxes_scrollbar(self.listboxes, self.scrollbar)
        insert_stats(self.client_name, self.listboxes)

    def place_widgets(self):
        self.header.place(relx=HEADER_X_POS, y=HEADER_Y_POS, anchor='n')
        self.return_button.place(relx=0.88, rely=0.85)
        self.date_list.place(relx=0.05, y=100, relwidth=0.15, relheight=0.7)
        self.time_list.place(relx=0.2, y=100, relwidth=0.12, relheight=0.7)
        self.websites_label.place(relx=0.29, y=65)
        self.websites_list.place(relx=0.29, y=100, relwidth=0.32, relheight=0.7)
        self.status_label.place(relx=0.61, y=65)
        self.status_list.place(relx=0.61, y=100, relwidth=0.2, relheight=0.7)
        self.scrollbar.pack(side="right", fill='y')
        self.clear_stats.place(relx=0.91, y=150, relwidth=150 / WINDOW_WIDTH, relheight=100 / WINDOW_HEIGHT,
                               anchor='n')
        self.refresh.pack(side=BOTTOM, pady=30)

    def bind_listboxes_to_press(self):
        self.date_list.bind('<Double-1>', lambda x: self.on_double_click(self.date_list))
        self.date_list.bind('<<ListboxSelect>>', lambda e: on_one_click(self.date_list, self.listboxes))
        self.time_list.bind('<Double-1>', lambda x: self.on_double_click(self.time_list))
        self.time_list.bind('<<ListboxSelect>>', lambda e: on_one_click(self.time_list, self.listboxes))
        self.websites_list.bind('<Double-1>', lambda x: self.on_double_click(self.websites_list))
        self.websites_list.bind('<<ListboxSelect>>', lambda e: on_one_click(self.websites_list, self.listboxes))
        self.status_list.bind('<Double-1>', lambda x: self.on_double_click(self.status_list))
        self.status_list.bind('<<ListboxSelect>>', lambda e: on_one_click(self.status_list, self.listboxes))

    def on_double_click(self, listbox):
        """when the user press double click on a website in the list
         checking which item he pressed on and calling the popup func"""
        try:
            index_of_press = listbox.curselection()
            items = on_double_click(self.listboxes, index_of_press)
            web_name = items[2]
            self.website_press_popup(web_name)
        except _tkinter.TclError:
            pass

    def website_press_popup(self, web_name):
        """
        the popup that is shown
        :param web_name:
        :return:
        """
        destroy_pop(self.pop)
        self.pop = define_pop(self, '540', '300')
        web_name_label = Label(self.pop, text=web_name, font=Font(size=23, weight="bold", underline=1), fg=DARK_BLUE)
        web_name_label.pack()
        add_button = Button(self.pop, text="Add\nWebsite\nTo Block", bg=BLACK, fg=WHITE, font=FONT,
                            command=lambda: [self.add_web_to_block(web_name), self.pop.destroy()])

        add_button.place(relx=0.1, y=50, relwidth=0.325, height=100)

        del_from_stats = Button(self.pop, text="Delete\nFuture\nShows", bg=BLACK, fg=WHITE, font=FONT,
                                command=lambda: [self.confirm_del_future_show(web_name), self.pop.destroy()])

        del_from_stats.place(relx=0.9, y=50, relwidth=0.325, height=100, anchor="ne")

        cancel_button = Button(self.pop, text=CANCEL, bg=GREY, font=FONT, command=lambda: self.pop.destroy())
        cancel_button.place(relx=0.5, rely=0.85, anchor=CENTER)

    def get_access_level(self):
        """
        get the access level of the client
        :return: the access level of the client
        """
        exe_command = f"SELECT access_level FROM '{CLIENTS_TABLE}' WHERE name='{self.client_name}'"
        BLOCKED_WEB_CURSOR.execute(exe_command)
        access_level = BLOCKED_WEB_CURSOR.fetchone()[0]
        return access_level

    def add_web_to_block(self, web_name):
        """
        adds the chosen website to block in the access level of the current client
        :param web_name: the chosen website's name
        :return: None
        """
        access_level = self.get_access_level()
        print(type(access_level))
        if access_level != '0':
            if not web_in_db(web_name, WEBSITES_TABLE, access_level):
                update_database(web_name, SPECIFIC, "add", access_level)  # add the website as blocked to the database
                # add the website to the blocked websites list of the app
                self.master.blocked_websites.append((web_name, SPECIFIC, access_level))
                messagebox.showinfo("", f"{web_name} added to block in access level {access_level}")
            else:
                messagebox.showwarning("Already Blocked", f"{web_name} is already blocked")
        else:
            messagebox.showerror("Error", "Client doesn't have access level.\n"
                                          "Please change the access level and try again.")

    def confirm_del_future_show(self, web_name):
        """
        confirm with the user that he wants to disable all the future DNS requests to the chosen website
        :param web_name: the name of the chosen website
        :return: None
        """
        if not web_in_db(web_name, NOT_SHOWN_WEBSITES_TABLE, None):
            confirm_box = messagebox.askquestion("Warning",
                                                 "Are you sure you want to disable all future shows of this website to"
                                                 f" all the clients?\nThis actions can not be undone.", icon="warning")
            if confirm_box == "yes":
                # disable the future shows of the website from the DNS statistics to all clients
                del_future_shows(web_name, self.master)
            else:
                self.website_press_popup(web_name)
        else:
            messagebox.showerror("Error", "This website's future shows already disabled")
        destroy_pop(self.pop)


def insert_stats(client, listboxes):
    """
    inserts the stats of the client from the database
    :param client: the name of the client
    :param listboxes: the listboxes of the stats
    :return:
    """
    exe_command = f"SELECT date, time, website, status FROM '{CLIENTS_STATS}' WHERE client='{client}'"
    BLOCKED_WEB_CURSOR.execute(exe_command)
    for row in BLOCKED_WEB_CURSOR:
        insert_row_to_listboxes(row, listboxes)


def insert_row_to_listboxes(row, listboxes):
    """
    insert the given row to the given listboxes by the order
    :param row: the stats details from the database
    :param listboxes: the listboxes of the stats
    :return: None
    """
    insert_date(row[0], listboxes[0])
    for i in range(1, len(row)):
        listboxes[i].insert(0, row[i])


def insert_date(date, date_listbox):
    """
    inserts the date of the stat. if it's today or yesterday writes it
    :param date: the date of the dns request
    :param date_listbox: the listbox to enter the date to
    :return: None
    """
    today = datetime.datetime.today().strftime('%d/%m/%Y')  # the date of today
    yesterday = (datetime.datetime.today() - datetime.timedelta(days=1)).strftime('%d/%m/%Y')  # the date of yesterday
    if date == today:  # if the date of the website visit was today
        date_listbox.insert(0, " Today")
    elif date == yesterday:  # if the date of the website visit was yesterday
        date_listbox.insert(0, " Yesterday")
    else:
        date_listbox.insert(0, date)


def refresh_stats(listboxes, client_name):
    """
    refresh the stats to match the database
    :param listboxes: the listboxes of the stats
    :param client_name: the name of the client
    :return: None
    """
    clear_listboxes(listboxes, CLIENTS_STATS)
    insert_stats(client_name, listboxes)

