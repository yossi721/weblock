# done ----------------------------------------

import _tkinter
from tkinter import *
from tkinter import messagebox
from tkinter.font import Font

from main_menu import MainMenu
from static_functions import *
from dns_server import start_dns_server


GREY = "#878C9F"
BLUE = "#4966C7"
WHITE = "white"
FONT = "Helvetica 20"
RETURN = "Return"
ENTRY_RELIEF = RAISED
ENTER = "<Return>"


class SignUp(Frame):
    """the frame that you go into if you want to create a new network"""

    def __init__(self, master, last_frame, sock):
        """
        the constructor function of the class
        :param master: the master of the class (the App class object)
        :param last_frame: the frame that was on the screen before the current frame
        """
        Frame.__init__(self, master)
        self.master = master
        self.last_frame = last_frame
        self.sock = sock
        self.return_button = Button(self, text=RETURN, font=FONT, bg="green",
                                    command=lambda: [master.switch_frame(last_frame.last_frame, last_frame.__class__),
                                                     self.sock.close()])

        self.canvas = Canvas(self, bg=GREY)

        self.enter_details_label = Label(self, text="Please enter your username and password:", font=FONT).pack()

        self.username = Label(self.canvas, text="Username:", bg=GREY, font=Font(size=14))
        self.enter_username = Entry(self.canvas, font=Font(size=16), relief=ENTRY_RELIEF)

        self.password = Label(self.canvas, text="Password:", bg=GREY, font=Font(size=14))
        self.enter_password = Entry(self.canvas, font=Font(size=16), show="*", relief=ENTRY_RELIEF)

        self.confirm_password_label = Label(self.canvas, text="Confirm Password:", bg=GREY, font=Font(size=14))

        self.confirm_password_entry = Entry(self.canvas, font=Font(size=16), show="*", relief=ENTRY_RELIEF)

        self.create_network_button = Button(self.canvas, text="Create Network", bg=BLUE, fg=WHITE, font=Font(size=16),
                                            command=lambda: self.check_valid_details())

        master.bind(ENTER, lambda event=None: self.create_network_button.invoke())
        self.place_widgets()

    def place_widgets(self):
        self.return_button.place(relx=0.88, rely=0.85)
        self.canvas.place(relx=0.5, y=30, width=500, height=500, anchor='n')
        self.username.place(x=100, y=35)
        self.enter_username.place(x=100, y=65, width=300, height=45)
        self.password.place(x=100, y=130)
        self.enter_password.place(x=100, y=160, width=300, height=45)
        self.confirm_password_label.place(x=100, y=225)
        self.confirm_password_entry.place(x=100, y=255, width=300, height=45)
        self.create_network_button.place(relx=0.5, rely=0.85, width=200, anchor=CENTER)

    def check_valid_details(self):
        """
        check if the details the user entered are valid
        :return: None
        """
        name = self.enter_username.get()  # get the name the the user entered
        password1 = self.enter_password.get()  # get the password the the user entered
        password2 = self.confirm_password_entry.get()  # get the password confirm the the user entered
        name = check_valid_name(name)  # check if the name that the user entered is in the valid form
        if name:  # if the name is in the valid form (if it's not the name gets false)
            if valid_password_form(password1):  # check if the password is in the valid form
                if check_match_pass(password1, password2):  # if the password and the password confirm are the same
                    if self.sock:  # if the socket is not none
                        send_user_details(self.sock, SIGN_UP, name, password1)  # send the details to the admin server
                        close_socket(self.sock)  # close the socket with the admin server
                        create_network(self.master)  # create the network as a manager


def create_network(master):
    """
    when user presses create network creates all the databases and moves to the network frame
    :param master: the object of the App class
    :return: None
    """
    create_access_levels_tables()
    create_clients_table()
    create_not_shown_table()
    create_client_stats_table()
    start_dns_server(master)
    master.switch_frame(None, MainMenu)
    master.start_socket()


def create_access_levels_tables():

    exe_command = "CREATE TABLE IF NOT EXISTS '" + WEBSITES_TABLE + "'(\n" \
                                                                    "url STRING,\n" \
                                                                    "specific_or_general STRING,\n" \
                                                                    "access_level CHAR\n" \
                                                                    ");"
    BLOCKED_WEB_CURSOR.execute(exe_command)


def create_clients_table():
    exe_command = "CREATE TABLE IF NOT EXISTS '" + CLIENTS_TABLE + "'(\n" \
                                                                   "name STRING,\n" \
                                                                   "access_level CHAR,\n" \
                                                                   "mac STRING PRIMARY KEY\n" \
                                                                   ");"
    BLOCKED_WEB_CURSOR.execute(exe_command)


def create_client_stats_table():
    exe_command = "CREATE TABLE IF NOT EXISTS '" + CLIENTS_STATS + "'(\n" \
                                                                   "date STRING,\n" \
                                                                   "time STRING,\n " \
                                                                   "website STRING,\n" \
                                                                   "status STRING,\n" \
                                                                   "client STRING\n" \
                                                                   ");"
    BLOCKED_WEB_CURSOR.execute(exe_command)


def create_not_shown_table():
    """
    websites that won't be shown in the client's statistics
    :return: None
    """
    exe_command = f"CREATE TABLE IF NOT EXISTS '{NOT_SHOWN_WEBSITES_TABLE}'(\n" \
                  "url STRING PRIMARY KEY\n" \
                  ");"
    BLOCKED_WEB_CURSOR.execute(exe_command)


def check_match_pass(password1, password2):
    """
    checks if the two given password are the same. if they are returns true, else returns false
    :param password1: the first password
    :param password2: the second password
    :return: True if the password match, False if not
    """
    if password2 == "":
        messagebox.showwarning("", "Please confirm the password")
        return False
    if password1 != password2:
        messagebox.showwarning("", "Passwords does not match")
        return False
    return True
