# done ----------------------------------------

import _tkinter
from tkinter import *
from tkinter import messagebox
from tkinter.font import Font
from static_functions import *
from check_connection import *

GREY = "#878C9F"
BLUE = "#4966C7"
WHITE = "white"
FONT = "Helvetica 20"
RETURN = "Return"
ENTRY_RELIEF = RAISED


class JoinNetworkWindow(Frame):
    def __init__(self, master, last_frame):
        """
        the constructor function of the class
        :param master: the master of the class (the App class object)
        :param last_frame: the frame that was on the screen before the current frame
        """
        Frame.__init__(self, master)
        self.master = master
        self.last_frame = last_frame
        self.pop = None  # the popup that is on the screen
        self.return_button = Button(self, text=RETURN, font=FONT, bg="green",
                                    command=lambda: master.switch_frame(last_frame.last_frame, last_frame.__class__))

        self.canvas = Canvas(self, bg=GREY)

        self.enter_details_label = Label(self, text="Please enter your name and main computer address:",
                                         font=FONT).pack()
        self.username = Label(self.canvas, text="Name:", bg=GREY, font=Font(size=14))

        self.enter_username = Entry(self.canvas, font=Font(size=16), relief=ENTRY_RELIEF)

        self.main_address = Label(self.canvas, text="Main Computer Address:", bg=GREY, font=Font(size=14))

        self.enter_address = Entry(self.canvas, font=Font(size=16), relief=ENTRY_RELIEF)

        self.join_button = Button(self.canvas, text="Connect", bg=BLUE, fg=WHITE, font=Font(size=16),
                                  command=lambda: self.send_name_to_server())

        master.bind(ENTER, lambda event=None: self.join_button.invoke())
        self.place_widgets()

    def place_widgets(self):
        self.return_button.place(relx=0.88, rely=0.85)
        self.canvas.place(relx=0.5, y=30, width=500, height=400, anchor='n')
        self.username.place(x=100, y=35)
        self.enter_username.place(x=100, y=65, width=300, height=45)
        self.main_address.place(x=100, y=130)
        self.enter_address.place(x=100, y=160, width=300, height=45)
        self.join_button.place(relx=0.5, rely=0.85, width=120, anchor=CENTER)

    def send_name_to_server(self):
        """
        checking if the name is valid. if it is - sending the name and mac to the ip address that is written in the
         entry box (if it's valid)
        :return: None
        """
        name = self.enter_username.get()  # get the name that the client entered
        name = check_valid_name(name)  # checks if the name is valid
        if name:  # if the name is valid
            server_address = self.enter_address.get()  # get the server address that the client entered
            server_address = valid_ip(server_address)  # checks if the ip the client entered is valid ip
            print(name + "  " + server_address)
            if server_address != "":
                sock = connect_to_server(server_address)  # try to connect to the DNS Proxy
                print(sock)
                if not sock or send_details_and_recv(sock, name, server_address, self.master) == CONNECTION_FAILED:
                    messagebox.showwarning("Warning", "Can not connect to the server.")


def send_details_and_recv(sock, name, server_address, app):
    """
    sending the mac address and the user name to the server
    :param sock: the socket connection with the DNS Proxy
    :param name: the name that was entered
    :param server_address: the address of the DNS Proxy that was entered
    :param app: the object of the App class
    :return: connection failed if there was a error, True if the join succeeded, False if not
    """
    try:
        sock.send(MY_MAC.encode())
        sock.send(name.encode())
        response = sock.recv(1024).decode()
        response_bool = check_response(response)
        if response_bool:
            clear_dns_cache()
            load_dns_thread = Thread(target=load_dns, args=(app, server_address), daemon=True)
            load_dns_thread.start()
            receive_msg_thread = Thread(target=receive_msg, args=(app, sock, server_address), daemon=True)
            receive_msg_thread.start()
    except Exception:
        return CONNECTION_FAILED
    return response_bool


