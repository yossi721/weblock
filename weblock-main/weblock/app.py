# done ----------------------------------------

import _tkinter
from tkinter import *
from tkinter import messagebox
from tkinter.font import Font
from socket import *
from threading import Thread
import sqlite3
import dns.resolver
import ctypes
import sys


from static_functions import *
from view_access_levels import ViewAccessLevels
from start_window import StartWindow
from main_menu import MainMenu
from view_clients import ViewClientsConnected
from check_connection import check_dns


APP_NAME = "Weblock"
WINDOW_HEIGHT = 700
WINDOW_WIDTH = 1000
SMALL_WINDOW_HEIGHT = 400
SMALL_WINDOW_WIDTH = 450
host_name = gethostname()
MY_IP = gethostbyname(host_name)
HOST = "127.0.0.1"
LOGIN_SERVER_IP = HOST # שמים את הכתובת של המחשב שמשמש כשרת ,כתובת ה ip של המחשב המשמש כשרת - button addres
LOGIN_SERVER_PORT = 54321
PORT = 23456
ENTER = "<Return>"
CHECKING_CONNECTION_MSG = ">>>???<<<"
ALREADY_CONNECTED = "already connected"
NOT_ALREADY_CONNECTED = "not already connected"
CONNECTION_FAILED = "&*)(#$%%*&$"
SERVER_CLOSED = "^*&%$#(^#&*$(^"
BLOCKED_WEB_DB = sqlite3.connect('app.db', check_same_thread=False)
BLOCKED_WEB_CURSOR = BLOCKED_WEB_DB.cursor()
SUCCEED_MSG = 'ok'
STARTING_ACCESS_LEVEL = '0'
NOT_SHOWN_WEBSITES_TABLE = 'not shown websites'
WEBSITES_TABLE = "websites blocked"
dns_resolver = dns.resolver.Resolver()
MY_DNS_SERVER = dns_resolver.nameservers[0]


class App(Tk):
    def __init__(self):
        """
        the constructor function of the class
        """
        Tk.__init__(self)
        self.frame = None
        self.switch_frame(self, StartWindow)
        self.server_socket = None
        self.client_and_conns = {}  # dictionary of all the clients connected to the main computer and their socket
        # with the main computer
        self.blocked_websites = []  # list of the websites that are blocked. every element is a tuple that has the
        # name of the website, the way it is blocked (specific/general), and the access level it is blocked in
        self.not_shown_websites = []  # the list of the websites that are not shown in the clients statistics
     
    
    def switch_frame(self, last_frame, new_frame_class):
        """
        switches between different frames in the app
        :param last_frame: the last frame that was on the screen
        :param new_frame_class: the new frame to show on the screen
        :return: None
        """
        self.unbind(ENTER)
        new_frame = new_frame_class(self, last_frame)  # create object of the new frame class
        if self.frame is not None:
            self.frame.destroy()  # destroys the previous frame
        print(new_frame)
        if isinstance(new_frame, MainMenu) or isinstance(new_frame, ViewAccessLevels):
            define_app_size(self, SMALL_WINDOW_WIDTH, SMALL_WINDOW_HEIGHT)
            self.resizable(False, False)
        else:
            self.resizable(True, True)
            define_app_size(self, WINDOW_WIDTH, WINDOW_HEIGHT)
        self.frame = new_frame
        self.frame.pack(fill="both", expand=True)  # packing the new frame to make it be on all the app window

    def switch_frame_arg(self, last_frame, new_frame_class, arg):
        """
        switches between frames that get argument
        :param last_frame: the last frame that was on the screen
        :param new_frame_class: the new frame to show on the screen
        :param arg: the argument that the new frame class need to get
        :return: None
        """
        self.resizable(True, True)
        define_app_size(self, WINDOW_WIDTH, WINDOW_HEIGHT)
        self.unbind(ENTER)
        new_frame = new_frame_class(self, last_frame, arg)
        self.frame.destroy()
        self.frame = new_frame
        self.frame.pack(fill="both", expand=True)

    def start_socket(self):
        """
        starting a new socket for the DNS Proxy
        :return: None
        """
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind((MY_IP, PORT))
        self.server_socket.listen(2)
        Thread(target=self.accept_new_clients, daemon=True).start()

    def accept_new_clients(self):
        """
        waiting for new computers to connect to the DNS Proxy, check their action, and act according to the action
        :return: None
        """
        socket_open = True  # variable to tell if the socket is open or not
        while socket_open:
            try:
                conn, addr = self.server_socket.accept()  # accept connection from new computer
                mac = conn.recv(1024).decode()
                name = conn.recv(1024).decode()
                # if the computer is checking if it is connected to the DNS Proxy
                if name == CHECKING_CONNECTION_MSG:
                    name = check_already_connected(mac)
                    # if the name exists for the given mac puts the new connection with the client in the dictionery
                    if name:
                        self.client_and_conns[name] = conn
                        conn.send(ALREADY_CONNECTED.encode())
                    # otherwise computer is not connected which means the manager removed him
                    else:
                        conn.send(NOT_ALREADY_CONNECTED.encode())
                # if the computer isn't already connected or the name not already exists
                elif not check_client_exists_db(conn, name, mac):
                    conn.send(SUCCEED_MSG.encode())
                    self.client_to_dbs(name, mac)
                    name_and_conn = {name: conn}  # dict of the name of the user and the socket connection with him
                    self.client_and_conns.update(name_and_conn)
                    messagebox.showinfo("Succeed", f"{name} has connected")
                    # conn.close()
            except OSError:
                socket_open = False

    def client_to_dbs(self, name, mac):
        """
        adds the client to the database
        :param name: the name of the client
        :param mac: the mac address of the client
        :return: None
        """
        add_client_to_db(name, mac)
        # if the frame is the frame of the computers connected updates the list of the computers connected
        if isinstance(self.frame, ViewClientsConnected):
            self.add_client_to_listbox(name)

    def add_client_to_listbox(self, name):
        """
        adds the client and the default access level to the lists on the screen
        :param name: the name of the client
        :return: None
        """
        self.frame.clients_list.insert(0, name)
        self.frame.access_levels_list.insert(0, STARTING_ACCESS_LEVEL)

    def set_blocked_webs(self):
        """
        sets the list of the blocked website from the database
        :return: None
        """
        exe_command = f"SELECT * FROM '{WEBSITES_TABLE}'"
        BLOCKED_WEB_CURSOR.execute(exe_command)
        blocked_web_lst = BLOCKED_WEB_CURSOR.fetchall()
        self.blocked_websites = blocked_web_lst

    def set_not_shown(self):
        """
        sets the list of the websites that are not shown in the statistics
        :return: none
        """
        exe_command = f"SELECT url FROM '{NOT_SHOWN_WEBSITES_TABLE}'"
        BLOCKED_WEB_CURSOR.execute(exe_command)
        not_shown_lst = BLOCKED_WEB_CURSOR.fetchall()
        self.not_shown_websites = not_shown_lst

def before_closing():
    print("bye")# if i am a user and my DNS changed, then change it back to defulat.
    


def start_app():
    app = App()
    app.protocol("WM_DELETE_WINDOW", before_closing)

    app.title(APP_NAME)
    app.resizable(False, False)
    app.mainloop()


def is_admin():
    """
    checks if the user has admin privilege level in the computer
    :return: True if the user is admin, False if not
    """
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except:
        return False


def main():
    if is_admin():
        if not check_dns(MY_DNS_SERVER):
            start_app()
    else:
        # Re-run the program with admin rights
        ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, " ".join(sys.argv), None, 1)


if __name__ == '__main__':
    print("hello")
    main()
