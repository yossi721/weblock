# done ----------------------------------
import _tkinter
from tkinter import *
from tkinter import messagebox
from tkinter.font import Font

from static_functions import *
from access_level import AccessLevel

HEADER_FONT = "Algerian 24 bold"
HEADER_COLOR = "dark slate gray"
HEADER_X_POS = 0.5
HEADER_Y_POS = 6
GREY = "#878C9F"
WHITE = "white"
FONT = "Helvetica 20"
RETURN = "Return"


class ViewAccessLevels(Frame):
    def __init__(self, master, last_frame):
        """
        the constructor function of the class
        :param master: the master of the class (the App class object)
        :param last_frame: the frame that was on the screen before the current frame
        """
        Frame.__init__(self, master)
        self.last_frame = last_frame
        self.header = Label(self, text="Access Levels", font=HEADER_FONT, fg=HEADER_COLOR)
        self.return_button = Button(self, text=RETURN, font=FONT, bg="green",
                                    command=lambda: master.switch_frame(last_frame.last_frame, last_frame.__class__))
        self.access_level_1 = Button(self, text="Access Level 1", font=FONT, bg=GREY, fg=WHITE,
                                     command=lambda: master.switch_frame_arg(self, AccessLevel, "1"))
        self.access_level_2 = Button(self, text="Access Level 2", font=FONT, bg=GREY, fg=WHITE,
                                     command=lambda: master.switch_frame_arg(self, AccessLevel, "2"))
        self.access_level_3 = Button(self, text="Access Level 3", font=FONT, bg=GREY, fg=WHITE,
                                     command=lambda: master.switch_frame_arg(self, AccessLevel, "3"))
        self.place_widgets()

    def place_widgets(self):
        self.header.place(relx=HEADER_X_POS, y=HEADER_Y_POS, anchor='n')
        self.return_button.place(relx=0.5, y=320, anchor='n')
        self.access_level_1.place(relx=0.5, y=80, anchor='n')
        self.access_level_2.place(relx=0.5, y=150, anchor='n')
        self.access_level_3.place(relx=0.5, y=220, anchor='n')
