# done ----------------------------------------

import _tkinter
from threading import Thread

from plyer import notification

from static_functions import *
CHECKING_CONNECTION_MSG = ">>>???<<<"
ALREADY_CONNECTED = "already connected"
DISCONNECTED = "disconnected"
# NOT_ALREADY_CONNECTED = "not already connected"


class CheckIfConnected(Tk):
    def __init__(self, sock, dns_server_addr):
        Tk.__init__(self)
        self.sock = sock
        self.dns_server_addr = dns_server_addr
        self.send_connection_check()

    def send_connection_check(self):
        self.sock.send(MY_MAC.encode())
        self.sock.send(CHECKING_CONNECTION_MSG.encode())
        response = self.sock.recv(1024).decode()
        self.check_if_connected(response)

    def check_if_connected(self, response):
        """
        check if the computer is removed from the DNS Proxy
        :param response: the response from the DNS Proxy
        :return: None
        """
        if response == ALREADY_CONNECTED:
            clear_dns_cache()
            receive_msg_thread = Thread(target=receive_msg, args=(self, self.sock, self.dns_server_addr), daemon=True)
            receive_msg_thread.start()
        else:
            Thread(target=handle_msg, args=(self, DISCONNECTED), daemon=True).start()


def check_dns(dns_server_addr):
    """
    checks if the computer is connected to a dns server
    :param dns_server_addr: the address of the dns server of the computer
    :return: True if connected, False if not
    """
    sock = connect_to_server(dns_server_addr)
    if sock:
        app = CheckIfConnected(sock, dns_server_addr)
        app.withdraw()
        app.mainloop()
        return True
    else:
        return False


def clear_dns_cache():
    """
    clears the computer's dns cache
    :return: None
    """
    command = f'ipconfig /flushdns'
    os.system(command)


def receive_msg(app, sock, dns_server_addr):
    """
    receiving msg from the main computer and handles it
    :param app: the object of the App class
    :param sock: the socket connection with the DNS Proxy
    :param dns_server_addr: the address of the DNS Proxy
    :return: None
    """
    stop = False
    while sock.fileno() != -1 and not stop:  # while the server is open
        print("reach")
        print(sock)
        try:
            msg_length = sock.recv(BUFFER_SIZE).decode()  # receive the length of the massage
            msg_length = int(msg_length)  # change the length to integer
            msg = sock.recv(msg_length).decode()  # receive the massage from DNS Proxy
            handle_msg(app, msg)  # handle the massage
        except ConnectionResetError as e:
            if e.errno == 10054:  # if the connection as stopped
                messagebox.showwarning("Warning", "The server has been closed.\n"
                                                  "You will not have any connection to the internet until it turned "
                                                  "on again!")
                stop = True
        except Exception as e:
            print(e)
    try_reconnect(dns_server_addr)  # try to reconnect to the DNS Proxy
    app.destroy()


def handle_msg(app, msg):
    """
    doing the correct action for the received msg. if the msg is disconnected- defining to the computer the its
    dns server is google dns server and closes the program. otherwise (the msg is blocked) showing the the website is
    blocked in windows notification
    :param app: the object of the App class
    :param msg: the massage from the DNS Proxy
    :return: None
    """
    if msg == DISCONNECTED:
        load_dns(app, GOOGLE_IP)
        app.destroy()
        sys.exit(-1)
    else:
        notification.notify(title="Blocked", message=msg)


def try_reconnect(dns_server_addr):
    """
    try to reconnect to the DNS Proxy
    :param dns_server_addr: the address of the dns server
    :return: None
    """
    while not check_dns(dns_server_addr):
        pass


def load_dns(app, ip):
    """
    loading screen when loading the dns server
    :param app: the object of the App class
    :param ip: the ip address to define dns server as
    :return: None
    """
    delete_all(app)
    app.minsize(0, 0)
    app.geometry(f'400x60+{POP_X_START}+{POP_Y_START}')
    app.resizable(False, False)
    app.deiconify()

    loading_msg = "Please wait while connecting to the dns server"
    Label(app, text=loading_msg, font=Font(size=14)).pack()

    progress = Progressbar(app, orient=HORIZONTAL, length=100, mode='determinate')
    progress.pack()
    define_dns_proxy(app, progress, ip)


def update_bar(app, progress_bar, percentage, ip):
    """
    updates the loading bar to the correct percentage
    :param app: the object of the App class
    :param progress_bar: the progress bar to update
    :param percentage: the percentage of the loading
    :param ip: the ip address to define dns server as
    :return: None
    """
    progress_bar['value'] = percentage
    app.update_idletasks()  # update the progress bar
    if percentage == 100:  # check if the loading as finished
        close_loading(app, ip)


def close_loading(app, ip):
    """
    showing the the loading is finished with message box and disappearing the app window
    :param app: the object of the App class
    :param ip: the ip address that was defined as dns server
    :return: None
    """
    app.unbind(ENTER)
    messagebox.showinfo("Succeed", "You are now connected to " + ip)
    app.withdraw()


def define_dns_proxy(app, progress_bar, ip):
    """
    defining the dns server for the computer. The define needs to be to all of the computer interfaces. the function
    gets all the interfaces of the computer and defining dns server to each one, and the percentage of the loading is
    shown according to the number of interfaces. every time a interface dns server is defined the percentage rise
    :param app: the object of the App class
    :param progress_bar: the progress bar to update
    :param ip: the ip address to define dns server as
    :return: None
    important: changing the client DNS to the server DNS 
    """
    addrs = psutil.net_if_addrs()  # gets the interfaces of the computer
    percentage = 0
    percentage_change = (100 / (len(addrs) + 1))  # defining by how much the percentage will rise
    # moves over all the interfaces and defining their dns server, and updating the percentage
    for interface in addrs.keys():
        define_interface_dns(interface, ip)
        percentage = percentage + percentage_change
        update_bar(app, progress_bar, percentage, ip)
    time.sleep(3)
    update_bar(app, progress_bar, 100, ip)


def define_interface_dns(interface, ip):
    """
    defining to the given interface the given ip as dns server
    :param interface: the interface of the computer to define the dns server to
    :param ip: the ip address to define the dns server as
    :return: None
    """
    command = f'netsh interface ip set dns "{interface}" static {ip}'
    os.system(command)
