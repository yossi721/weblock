import _tkinter
from tkinter import *
from tkinter import messagebox
from tkinter.font import Font
import PIL.Image
import PIL.ImageTk
from static_functions import *
from join import JoinNetworkWindow
from login import Login
from sign_up import SignUp


IMAGE_PATH = "network.jpg"
GREY = "#878C9F"
WHITE = "white"
BLACK = "black"
FONT = "Helvetica 20"
BUTTON_WIDTH = 250
BUTTON_HEIGHT = 60


class StartWindow(Frame):
    def __init__(self, master, last_frame):
        """
        the constructor function of the class
        :param master: the master of the class (the App class object)
        :param last_frame: the frame that was on the screen before the current frame
        """
        Frame.__init__(self, master)
        self.last_frame = last_frame  # the frame that was before the current frame
        self.master = master
        img = get_img()  # get the image
        self.img_label = Label(self, image=img)
        self.img_label.img = img  # Keep a reference in case this code put is in a function
        self.welcome_label = Label(self, text=f"Welcome To {APP_NAME}!", font=FONT, bg="#7A6C9F")
        self.login_button = Button(self, text="Log In", font=FONT, bg=GREY,
                                   command=lambda: self.try_connect(Login))
        self.join_network_button = Button(self, text="Connect To Server", font=FONT, bg=GREY,
                                          command=lambda: master.switch_frame(self, JoinNetworkWindow))
        self.already_have_account_button = Button(self, text="Don't have an account? Sign Up here",
                                                  bg=WHITE, fg=BLACK, font=Font(size=14),
                                                  command=lambda: self.try_connect(SignUp))
        self.place_widgets()

    def place_widgets(self):
        self.img_label.place(relx=0.5, rely=0.5, anchor='center')  # Place label in center of parent.
        self.welcome_label.place(relx=0.5, y=0.3 * WINDOW_HEIGHT, anchor=CENTER)
        self.login_button.place(relx=0.49, y=0.5 * WINDOW_HEIGHT, width=BUTTON_WIDTH, height=BUTTON_HEIGHT, anchor="e")
        self.join_network_button.place(relx=0.51, y=0.5 * WINDOW_HEIGHT, width=BUTTON_WIDTH, height=BUTTON_HEIGHT,
                                       anchor="w")
        self.already_have_account_button.place(relx=0.5, y=415, anchor=CENTER)

    def try_connect(self, frame):
        """tries to connect to the login server. if succeeds moves to the given frame"""
        sock = connect_admin_server()
        if sock:
            self.master.switch_frame_arg(self, frame, sock)


def get_img():
    # open the image and resize it to the size of the app
    image1 = PIL.Image.open(IMAGE_PATH).resize((WINDOW_WIDTH, WINDOW_HEIGHT), PIL.Image.ANTIALIAS)
    img = PIL.ImageTk.PhotoImage(image1)  # make the image fit to tkinter
    return img


def connect_admin_server():
    """
    connect to the admin server
    :return: the socket if the connection succeed, None if not
    """
    try:
        # try to connect to the admin server
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((LOGIN_SERVER_IP, LOGIN_SERVER_PORT))
        return sock
    except ConnectionRefusedError:
        messagebox.showwarning("Error", "Can not connect to server. Please try again later.")
