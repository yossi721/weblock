from socket import *
from flask import Flask, render_template

host_name = gethostname()
MY_IP = gethostbyname(host_name)

app = Flask(__name__)


@app.route("/")
def block():
    return render_template("block_website.html", website="this website")


def start_flask():
    print("start flask")
    app.run(host=MY_IP, port=80)


if __name__ == '__main__':
    app.run()
