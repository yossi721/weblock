# done ----------------------------------------

import _tkinter
from tkinter import *
from tkinter import messagebox
from tkinter.font import Font
from static_functions import *

GREY = "#878C9F"
WHITE = "white"


class Address(Frame):
    def __init__(self, master, last_frame):
        """
        the constructor function of the class
        :param master: the master of the class (the App class object)
        :param last_frame: the frame that was on the screen before the current frame
        """
        Frame.__init__(self, master)
        self.last_frame = last_frame
        self.canvas = Canvas(self, bg="#302E38")
        self.your_address_label = Label(self.canvas, text=f"Your Address Is:", font=Font(size=21), bg="#302E38",
                                        fg=WHITE)
        self.the_address = Label(self.canvas, text=MY_IP, font=Font(size=23, weight="bold", underline=1), bg="#302E38",
                                 fg=WHITE)

        self.continue_button = Button(self.canvas, text="Continue", font=Font(size=16), bg=GREY, fg=WHITE,
                                      command=lambda: master.switch_frame(last_frame.last_frame, last_frame.__class__))
        self.place_widgets()

    def place_widgets(self):
        self.canvas.place(relx=0.5, y=WINDOW_HEIGHT * 0.3, width=400, height=340, anchor=CENTER)
        self.your_address_label.place(relx=0.5, rely=0.3, anchor=CENTER)
        self.the_address.place(relx=0.5, rely=0.4, anchor=CENTER)
        self.continue_button.place(relx=0.5, rely=0.95, width=150, anchor='s')
