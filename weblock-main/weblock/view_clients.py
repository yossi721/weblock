# done ------------------------------------
import _tkinter
from tkinter import *
from tkinter import messagebox
from tkinter.font import Font
from static_functions import *
from stats import ClientStats

HEADER_FONT = "Algerian 24 bold"
HEADER_COLOR = "dark slate gray"
HEADER_X_POS = 0.5
HEADER_Y_POS = 6
GREY = "#878C9F"
BLUE = "#4966C7"
WHITE = "white"
DARK_BLUE = "#152A7A"
BLACK = "black"
FONT = "Helvetica 20"
RETURN = "Return"
CANCEL = "Cancel"
ENTRY_RELIEF = RAISED


class ViewClientsConnected(Frame):
    """the window of all the clients connected to the server"""

    def __init__(self, master, last_frame):
        """
        the constructor function of the class
        :param master: the master of the class (the App class object)
        :param last_frame: the frame that was on the screen before the current frame
        """
        Frame.__init__(self, master)
        self.last_frame = last_frame
        self.pop = None
        self.master = master
        self.header = Label(self, text="Users Connected", font=HEADER_FONT, fg=HEADER_COLOR)
        self.return_button = Button(self, text=RETURN, font=FONT, bg="green",
                                    command=lambda: master.switch_frame(last_frame.last_frame, last_frame.__class__))

        self.clients_label = Label(self, text="Clients:", fg=BLUE, font=Font(size=18, weight="bold"))

        self.clients_list = Listbox(self, bg=GREY, fg=WHITE, font=Font(size=16), activestyle='none',
                                    selectmode=SINGLE, exportselection=False, highlightthickness=0)

        self.access_level_label = Label(self, text="Access Level:", fg=BLUE,
                                        font=Font(size=18, weight="bold"))

        self.access_levels_list = Listbox(self, bg=GREY, fg=WHITE, font=Font(size=16), activestyle='none',
                                          selectmode=SINGLE, exportselection=False, highlightthickness=0)

        self.listboxes = [self.clients_list, self.access_levels_list]

        self.scrollbar = Scrollbar(self.access_levels_list, orient=VERTICAL,
                                   command=lambda e: on_vsb(e, self.listboxes))

        self.remove_client_button = Button(self, text="Disconnect\nA\nClient", bg=DARK_BLUE, fg=WHITE,
                                           font=Font(size=17), command=lambda: self.remove_client_popup())

        self.clear_clients = Button(self, text="Clear\nAll\nClients", bg=DARK_BLUE, fg=WHITE,
                                    font=Font(size=17), command=lambda: [destroy_pop(self.pop),
                                                                         confirm_clear(self.master, self.listboxes,
                                                                                       CLIENTS_TABLE, None)])

        self.place_widgets()
        self.bind_listboxes_to_press()  # bind all the listboxes to press
        bind_listboxes_scrollbar(self.listboxes, self.scrollbar)  # bind all the listboxes to the same scrollbar
        insert_data_to_listboxes(CLIENTS_TABLE, self.listboxes, None)

    def place_widgets(self):
        self.header.place(relx=HEADER_X_POS, y=HEADER_Y_POS, anchor='n')
        self.return_button.place(relx=0.88, rely=0.85)
        self.clients_label.place(relx=0.08, y=65)
        self.clients_list.place(relx=0.08, y=100, relwidth=0.5, relheight=0.7)
        self.access_level_label.place(relx=0.58, y=65)
        self.access_levels_list.place(relx=0.58, y=100, relwidth=0.2, relheight=0.7)
        self.scrollbar.pack(side="right", fill='y')
        self.remove_client_button.place(relx=0.88, y=100, relwidth=150 / WINDOW_WIDTH, relheight=100 / WINDOW_HEIGHT,
                                        anchor='n')
        self.clear_clients.place(relx=0.88, y=250, relwidth=150 / WINDOW_WIDTH, relheight=100 / WINDOW_HEIGHT,
                                 anchor='n')

    def bind_listboxes_to_press(self):
        self.clients_list.bind('<Double-1>', lambda x: self.on_double_click(self.clients_list))
        self.clients_list.bind('<<ListboxSelect>>',
                               lambda e: on_one_click(self.clients_list, self.listboxes))
        self.access_levels_list.bind('<Double-1>',
                                     lambda x: self.on_double_click(self.access_levels_list))
        self.access_levels_list.bind('<<ListboxSelect>>',
                                     lambda e: on_one_click(self.access_levels_list,
                                                            self.listboxes))

    def on_double_click(self, listbox):
        """when the user press double click on a website in the list
         checking which item he pressed on and calling the popup func"""
        try:
            index_of_press = listbox.curselection()
            items = on_double_click(self.listboxes, index_of_press)
            client_name = items[0]
            access_level = items[1]
            self.client_pressed_popup(index_of_press, client_name)
        # except IndexError:
        except _tkinter.TclError:
            pass

    def client_pressed_popup(self, index_of_press, client_name):
        """
        pops a message to ask the client what he wishes to do
        :param index_of_press: the index of the press in the listbox
        :param client_name: the name of the client
        :return: None
        """
        # TODO ? make the popup pop from one press
        destroy_pop(self.pop)
        self.pop = define_pop(self, '600', '300')
        client_name_label = Label(self.pop, text=client_name, font=Font(size=23, weight="bold", underline=1),
                                  fg=DARK_BLUE)
        client_name_label.place(x=300, anchor="n")
        remove_button = Button(self.pop, text="Disconnect\nClient", bg=BLACK, fg=WHITE, font=FONT,
                               command=lambda: [self.confirm_remove_client(index_of_press, client_name, "press")])
        remove_button.place(x=25, y=50, width=145, height=100)

        change_button_text = f"Change\nAccess\nLevel"
        change_button = Button(self.pop, text=change_button_text, bg=BLACK, fg=WHITE, font=FONT,
                               command=lambda: [self.change_access_level_popup(index_of_press, client_name)])
        change_button.place(x=300, y=50, width=145, height=100, anchor="n")

        view_statistics_button = Button(self.pop, text="View\nStatistics", bg=BLACK, fg=WHITE, font=FONT,
                                        command=lambda: [self.master.switch_frame_arg(self, ClientStats, client_name)])
        view_statistics_button.place(x=575, y=50, width=145, height=100, anchor="ne")

        cancel_button = Button(self.pop, text=CANCEL, bg=GREY, font=FONT, command=lambda: self.pop.destroy())
        cancel_button.place(x=300, rely=0.85, anchor=CENTER)

    def remove_client_popup(self):
        """
        popup the is shown when remove client button is pressed
        :return: None
        """
        destroy_pop(self.pop)
        self.pop = define_pop(self, '500', '300')
        client_to_remove = Label(self.pop, text="Client To Remove: ", font=FONT)
        client_to_remove.place(x=10, y=10)
        enter_client_name = Entry(self.pop, font=Font(size=16), relief=ENTRY_RELIEF)
        enter_client_name.place(x=10, y=50, width=370, height=45)
        enter_client_name.focus()

        remove_button = Button(self.pop, text="Remove\nClient", font=FONT, bg=DARK_BLUE, fg=WHITE,
                               command=lambda: self.remove_client_by_name(enter_client_name))
        remove_button.place(x=270, y=160, anchor="w", width=110, height=90)
        self.pop.bind(ENTER, lambda e: [self.remove_client_by_name(enter_client_name)])
        cancel_button = Button(self.pop, text=CANCEL, bg=GREY, font=FONT, command=lambda: self.pop.destroy())
        cancel_button.place(x=230, y=160, anchor='e', width=110, height=90)

    def confirm_remove_client(self, index, client_name, how_remove):
        """
        confirm with the user that he really wants to remove a client.
         how remove says if the user removed the client by typing its name or by pressing on the client
        :param index: the index of the client in the listbox
        :param client_name: the name of the client
        :param how_remove: the way to remove (specific / general)
        :return: None
        """
        confirm_text = f"Are you sure you want to remove {client_name} from the clients connected?"
        confirm_box = messagebox.askquestion('Remove', confirm_text, icon='warning')
        if confirm_box == "yes":
            destroy_pop(self.pop)
            self.remove_client(index, client_name)
        else:
            if how_remove == "press":
                self.client_pressed_popup(index, client_name)
            elif how_remove == "typing":
                self.remove_client_popup()

    def remove_client_by_name(self, entry):
        """
        gets the name that was written in the remove client popup, finds it's index and removes him
        :param entry: the entry box
        :return: None
        """
        client_name = entry.get()
        if is_valid(client_name):
            if item_in_listbox(self.clients_list, client_name):
                index = self.clients_list.get(0, END).index(client_name)
                self.confirm_remove_client(index, client_name, "typing")
            else:
                messagebox.showerror("Error", f"{client_name} is not in the list.\n"
                                              f"Please make sure you typed the name correctly.")
                self.remove_client_popup()
        else:
            messagebox.showerror("Error", "Please enter a valid name")
            self.remove_client_popup()

    def remove_client(self, index, client_name):
        """
        removes the client from everything and sending him that he has been removed
        :param index: the index of the client in the listbox
        :param client_name: the name of the client
        :return: None
        """
        delete_item_by_index(self.listboxes, index)
        remove_client_from_db(client_name)
        remove_client_stats(client_name)
        disconnect_client(self.master, client_name)

    def change_access_level_popup(self, index, client_name):
        """
        the popup when change access level is pressed
        :param index: the index if the pressed client in the listbox
        :param client_name: the name of the client
        :return: None
        """
        destroy_pop(self.pop)
        self.pop = define_pop(self, '540', '300')
        client_name_label = Label(self.pop, text=f"Client: {client_name}",
                                  font=Font(size=23, weight="bold", underline=1))
        client_name_label.place(relx=0, y=0)

        change_to_label = Label(self.pop, text="Change To:", font=Font(size=23, weight="bold", underline=1))
        change_to_label.place(relx=0, y=80)

        access_level_1 = Button(self.pop, text="Access\nLevel\n1", font=Font(size=17), bg=GREY, fg=WHITE,
                                command=lambda: [self.change_access_level_lb(index, client_name, "1"),
                                                 self.pop.destroy()])
        access_level_1.place(relx=175 / 540, y=95, relwidth=0.17, height=75, anchor="w")

        access_level_2 = Button(self.pop, text="Access\nLevel\n2", font=Font(size=17), bg=GREY, fg=WHITE,
                                command=lambda: [self.change_access_level_lb(index, client_name, "2"),
                                                 self.pop.destroy()])
        access_level_2.place(relx=275 / 540, y=95, relwidth=0.17, height=75, anchor="w")

        access_level_3 = Button(self.pop, text="Access\nLevel\n3", font=Font(size=17), bg=GREY, fg=WHITE,
                                command=lambda: [self.change_access_level_lb(index, client_name, "3"),
                                                 self.pop.destroy()])
        access_level_3.place(relx=375 / 540, y=95, relwidth=0.17, height=75, anchor="w")

        cancel_button = Button(self.pop, text=CANCEL, bg=GREY, font=FONT,
                               command=lambda: [self.client_pressed_popup(index, client_name)])
        cancel_button.place(relx=0.5, y=250, anchor=CENTER)

    def change_access_level_lb(self, index, client_name, change_to):
        """
        changes the client's access level in the listbox
        :param index: the index of the client in the listbox
        :param client_name: the name of the client
        :param change_to: what access level to change to
        :return: None
        """
        self.access_levels_list.delete(index)
        self.access_levels_list.insert(index, change_to)
        self.access_levels_list.select_set(index)
        change_access_level_db(client_name, change_to)


def remove_client_from_db(client_name):
    """
    removes the client from the database
    :param client_name: the name of the client
    :return: None
    """
    exe_command = f"DELETE FROM '{CLIENTS_TABLE}' WHERE name='{client_name}'"
    BLOCKED_WEB_CURSOR.execute(exe_command)
    BLOCKED_WEB_DB.commit()


def remove_client_stats(client_name):
    """
    removes the client stats from the database
    :param client_name: the name of the client
    :return: None
    """
    exe_command = f"DELETE FROM '{CLIENTS_STATS}' WHERE client='{client_name}'"
    print(exe_command)
    BLOCKED_WEB_CURSOR.execute(exe_command)
    BLOCKED_WEB_DB.commit()


def change_access_level_db(client_name, change_to):
    """
    changes the client's access level in the database
    :param client_name: the name of the client
    :param change_to: what access level to change to
    :return: None
    """
    change_command = f"UPDATE '{CLIENTS_TABLE}' SET access_level='{change_to}'" \
                     f" WHERE name='{client_name}' "
    BLOCKED_WEB_CURSOR.execute(change_command)
    BLOCKED_WEB_DB.commit()

