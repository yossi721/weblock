# done ----------------------------------------

import _tkinter
from tkinter import *
from tkinter import messagebox
from tkinter.font import Font
from static_functions import *

HEADER_FONT = "Algerian 24 bold"
HEADER_COLOR = "dark slate gray"
HEADER_X_POS = 0.5
HEADER_Y_POS = 6
GREY = "#878C9F"
BLUE = "#4966C7"
WHITE = "white"
DARK_BLUE = "#152A7A"
BLACK = "black"
FONT = "Helvetica 20"
RETURN = "Return"
CANCEL = "Cancel"
ENTRY_RELIEF = RAISED


class AccessLevel(Frame):
    def __init__(self, master, last_frame, access_level):
        """
        the constructor function of the class
        :param master: the master of the class (the App class object)
        :param last_frame: the frame that was on the screen before the current frame
        :param access_level: the access level the the manager entered
        """
        Frame.__init__(self, master)
        self.last_frame = last_frame
        self.access_level = access_level  # the current access level
        self.pop = None  # the popup that is on the screen
        self.return_button = Button(self, text=RETURN, font=FONT, bg="green",
                                    command=lambda: master.switch_frame(last_frame.last_frame, last_frame.__class__))

        self.access_level_name = Label(self, text=f"Access Level {self.access_level}", font=HEADER_FONT,
                                       fg=HEADER_COLOR)

        self.websites_label = Label(self, text="Websites:", fg=BLUE, font=Font(size=18, weight="bold"))

        self.website_name_list = Listbox(self, bg=GREY, fg=WHITE, font=Font(size=16), activestyle='none',
                                         selectmode=SINGLE, exportselection=False, highlightthickness=0)

        self.specific_or_general_label = Label(self, text="Specific Or General:", fg=BLUE,
                                               font=Font(size=18, weight="bold"))

        self.specific_or_general_list = Listbox(self, bg=GREY, fg=WHITE, font=Font(size=16), activestyle='none',
                                                selectmode=SINGLE, exportselection=False, highlightthickness=0)

        self.listboxes = [self.website_name_list, self.specific_or_general_list]

        self.scrollbar = Scrollbar(self.specific_or_general_list, orient=VERTICAL,
                                   command=lambda e: on_vsb(e, self.listboxes))

        self.add_website_button = Button(self, text="Add Website\nTo Block", bg=DARK_BLUE, fg=WHITE, font=Font(size=17),
                                         command=lambda: [self.add_website_popup()])

        self.remove_website_button = Button(self, text="Remove\nWebsite From\n Block", bg=DARK_BLUE, fg=WHITE,
                                            font=Font(size=17), command=lambda: [self.remove_website_popup()])

        self.clear_websites = Button(self, text="Clear Blocked\nWebsites", bg=DARK_BLUE, fg=WHITE,
                                     font=Font(size=17),
                                     command=lambda: [destroy_pop(self.pop),
                                                      confirm_clear(self.master, self.listboxes,
                                                                    WEBSITES_TABLE, self.access_level)])
        self.bind_listboxes_to_press()
        self.place_widgets()
        bind_listboxes_scrollbar(self.listboxes, self.scrollbar)
        insert_data_to_listboxes(WEBSITES_TABLE, self.listboxes, self.access_level)

    def place_widgets(self):
        self.return_button.place(relx=0.88, rely=0.85)
        self.access_level_name.place(relx=HEADER_X_POS, y=HEADER_Y_POS, anchor='n')
        # self.websites_label.place(x=130, y=65)
        self.websites_label.place(relx=0.08, y=65)
        # self.website_name_list.place(x=130, y=100, width=400, relheight=0.7)
        self.website_name_list.place(relx=0.08, y=100, relwidth=0.4, relheight=0.7)
        # self.specific_or_general_label.place(x=530, y=65)
        self.specific_or_general_label.place(relx=0.48, y=65)
        # self.specific_or_general_list.place(x=530, y=100, width=300, relheight=0.7)
        self.specific_or_general_list.place(relx=0.48, y=100, relwidth=0.3, relheight=0.7)
        self.scrollbar.pack(side="right", fill='y')
        # self.add_website_button.place(x=915, y=100, anchor='n')
        self.add_website_button.place(relx=0.88, y=100, relwidth=150 / WINDOW_WIDTH, relheight=100 / WINDOW_HEIGHT,
                                      anchor='n')
        self.remove_website_button.place(relx=0.88, y=250, relwidth=150 / WINDOW_WIDTH, relheight=100 / WINDOW_HEIGHT,
                                         anchor='n')
        self.clear_websites.place(relx=0.88, y=400, relwidth=150 / WINDOW_WIDTH, relheight=100 / WINDOW_HEIGHT,
                                  anchor='n')

    def bind_listboxes_to_press(self):
        self.website_name_list.bind('<Double-1>', lambda x: self.on_double_click(self.website_name_list))

        self.website_name_list.bind('<<ListboxSelect>>',
                                    lambda e: on_one_click(self.website_name_list, self.listboxes))
        self.specific_or_general_list.bind('<Double-1>',
                                           lambda x: self.on_double_click(self.specific_or_general_list))

        self.specific_or_general_list.bind('<<ListboxSelect>>',
                                           lambda e: on_one_click(self.specific_or_general_list,
                                                                  self.listboxes))

    def on_double_click(self, listbox):
        """
        when the user press double click on a website in the list
         checking which item he pressed on and calling the popup func
        :param listbox: the listbox to bind double click to
        :return: None
        """
        try:
            index_of_press = listbox.curselection()
            items = on_double_click(self.listboxes, index_of_press)
            web_name = items[0]
            specific_or_general = items[1]
            self.website_press_popup(index_of_press, web_name, specific_or_general)
        except _tkinter.TclError:  # if pressed on nothing
            pass

    def website_press_popup(self, index_of_press, web_name, specific_or_general):
        """
        pops a message to ask the client what he wishes to do
        :param index_of_press: the index of the website pressed in the listbox
        :param web_name: the name of the pressed website
        :param specific_or_general: the way that the website is blocked
        :return: None
        """
        # TODO ? make the popup pop from one press
        destroy_pop(self.pop)
        self.pop = define_pop(self, '540', '300')
        web_name_label = Label(self.pop, text=web_name, font=Font(size=23, weight="bold", underline=1), fg=DARK_BLUE)
        # web_name_label.place(x=200, anchor="n")
        web_name_label.pack()

        remove_button = Button(self.pop, text="Remove\nFrom\nBlock", bg=BLACK, fg=WHITE, font=FONT,
                               command=lambda: [self.confirm_remove_web(index_of_press, web_name, specific_or_general),
                                                self.pop.destroy()])
        # remove_button.place(x=40, y=50, width=130, height=100)
        remove_button.place(relx=0.1, y=50, relwidth=0.325, height=100)

        change_to = self.change_to(specific_or_general)
        change_button_text = f"Change To\n{change_to}"
        change_button = Button(self.pop, text=change_button_text, bg=BLACK, fg=WHITE, font=FONT,
                               command=lambda: [self.change_specific_general(index_of_press, web_name, change_to),
                                                self.pop.destroy()])
        # change_button.place(x=360, y=50, width=130, height=100, anchor="ne")
        change_button.place(relx=0.9, y=50, relwidth=0.325, height=100, anchor="ne")

        cancel_button = Button(self.pop, text=CANCEL, bg=GREY, font=FONT, command=lambda: self.pop.destroy())
        # cancel_button.place(x=200, rely=0.85, anchor=CENTER)
        cancel_button.place(relx=0.5, rely=0.85, anchor=CENTER)

    @staticmethod
    def change_to(specific_or_general):
        """
        :param specific_or_general: the current way of block
        :return: the way of block to change to
        """
        if specific_or_general == "specific":
            change_to = GENERAL
        else:
            change_to = SPECIFIC
        return change_to

    def change_specific_general(self, index_of_press, web_name, change_to):
        """
        change the website specific or general status to the opposite of what it is now
        :param index_of_press: the index of the pressed website
        :param web_name: the name of the pressed website
        :param change_to: what way of block to change to
        :return: None
        """
        self.specific_or_general_list.delete(index_of_press)
        self.specific_or_general_list.insert(index_of_press, change_to)
        self.specific_or_general_list.select_set(index_of_press)
        update_database(web_name, change_to, "change", self.access_level)
        self.master.set_blocked_webs()

    def remove_website_popup(self):
        """
        if the remove website button was pressed
        :return: None
        """
        destroy_pop(self.pop)
        self.pop = define_pop(self, '500', '300')
        add_website_label = Label(self.pop, text="Website To Remove: ", font=FONT)
        add_website_label.place(x=10, y=10)
        enter_website = Entry(self.pop, font=Font(size=16), relief=ENTRY_RELIEF)
        enter_website.place(x=10, y=50, width=370, height=45)
        enter_website.focus()

        remove_general_button = Button(self.pop, text="Remove\nGeneral", font=FONT, bg=DARK_BLUE, fg=WHITE,
                                       command=lambda: [self.confirm_remove_general_specific(enter_website, GENERAL)])
                                                        # self.pop.destroy()])
        remove_general_button.place(x=150, y=160, anchor=CENTER)

        remove_specific_button = Button(self.pop, text="Remove\nSpecific", font=FONT, bg=DARK_BLUE, fg=WHITE,
                                        command=lambda: [self.confirm_remove_general_specific(enter_website, SPECIFIC)])
                                                         # self.pop.destroy()])
        remove_specific_button.place(x=350, y=160, anchor=CENTER)

        cancel_button = Button(self.pop, text=CANCEL, bg=GREY, font=FONT, command=lambda: self.pop.destroy())
        cancel_button.place(x=250, y=250, anchor=CENTER)

    def confirm_remove_general_specific(self, entry, how_remove):
        """
        confirms with the manager if he is sure he wants to remove what he chose from block
        :param entry: the entry box for the remove
        :param how_remove: the way to remove
        :return: None
        """
        web_name = entry.get()
        if is_valid(web_name):
            if how_remove == SPECIFIC:
                if item_in_listbox(self.website_name_list, web_name):
                    message_text = f"Are you sure you want to remove {web_name} from block?"
                else:
                    messagebox.showwarning("", f"{web_name} is not in the blocked list.\n"
                                               f"Please make sure you typed the name correctly.")
                    return False
            else:
                message_text = f"Are you sure you want to remove all website that has {web_name} in their name from " \
                               f"block? "

            confirm_box = messagebox.askquestion('Remove Website', message_text, icon='warning')
            if confirm_box == 'yes':
                self.remove_website_by_name(web_name, how_remove)
                self.pop.destroy()
            else:
                self.remove_website_popup()
        else:
            messagebox.showerror("Error", "Please enter valid website to remove.")
            self.remove_website_popup()

    def remove_website_by_name(self, web_name, how_remove):
        """
        moves over all the websites in the access level and if the websites needs to be removed passes the index
        in the listbox for another function
        :param web_name: the name of the website to remove
        :param how_remove: the way to remove
        :return: None
        """
        websites_blocked_count = self.website_name_list.size()  # the count of the blocked websites in the access level
        removed_count = 0  # counts how many website were removed if the user chose to remove general
        for index in range(websites_blocked_count - 1, -1, -1):
            check_web = self.website_name_list.get(index)
            if how_remove == SPECIFIC:
                if check_web == web_name:
                    removed_count += 1
                    self.remove_website_by_index(index, web_name, how_remove)
            elif how_remove == GENERAL:
                if web_name in check_web:
                    removed_count += 1
                    self.remove_website_by_index(index, web_name, how_remove)
        how_many_removed(how_remove, removed_count)

    def confirm_remove_web(self, index_of_press, web_name, specific_or_general):
        """
        confirms with the manager that he wants to remove the website from block
        :param index_of_press: the index of the pressed website inthe listbox
        :param web_name: the name of the website
        :param specific_or_general: the way that the website is blocked
        :return: None
        """
        confirm_text = "Are you sure you want to remove " + web_name + " from block?"
        confirm_box = messagebox.askquestion('Remove', confirm_text, icon='warning')
        if confirm_box == "yes":
            self.remove_website_by_index(index_of_press, web_name, specific_or_general)
        else:
            self.website_press_popup(index_of_press, web_name, specific_or_general)

    def remove_website_by_index(self, index, web_name, specific_or_general):
        """
        removes the given website from block
        :param index: the index of the pressed website in the listbox
        :param web_name: the name of the website
        :param specific_or_general: the way that the website is blocked
        :return: None
        """
        self.website_name_list.delete(index)
        self.specific_or_general_list.delete(index)
        update_database(web_name, specific_or_general, "remove", self.access_level)
        self.master.set_blocked_webs()

    def add_website_popup(self):
        """
        the popup that is shown when the manager presses add website button
        :return: None
        """
        destroy_pop(self.pop)
        self.pop = define_pop(self, '500', '300')
        add_website_label = Label(self.pop, text="Websites To Add:",
                                  font=Font(size=17))
        add_website_label.place(x=3, y=10)
        separate_label = Label(self.pop, text=" (separate different websites with space)", font=Font(size=12))
        separate_label.place(x=185, y=15)
        enter_website = Entry(self.pop, font=Font(size=16), relief=ENTRY_RELIEF)
        enter_website.place(x=10, y=50, width=370, height=45)
        enter_website.focus()
        block_general_button = Button(self.pop, text="Block\nGeneral", font=FONT, bg=DARK_BLUE, fg=WHITE,
                                      command=lambda: [self.add_website(enter_website, GENERAL), self.pop.destroy()])
        block_general_button.place(x=150, y=160, anchor=CENTER)

        block_specific_button = Button(self.pop, text="Block\nSpecific", font=FONT, bg=DARK_BLUE, fg=WHITE,
                                       command=lambda: [self.add_website(enter_website, SPECIFIC), self.pop.destroy()])
        block_specific_button.place(x=350, y=160, anchor=CENTER)

        cancel_button = Button(self.pop, text=CANCEL, bg=GREY, font=FONT, command=lambda: self.pop.destroy())
        cancel_button.place(x=250, y=250, anchor=CENTER)

    def add_website(self, entry, how_blocked):
        """
        adds website to the blocked list by the way the user chose
        :param entry: the entry box to add websites to block
        :param how_blocked: the way to block the added websites
        :return: None
        """
        websites = entry.get()
        websites_to_block_list = remove_none_string(websites.split(" "))
        for web in websites_to_block_list:
            if is_valid(web):
                if not web_in_db(web, WEBSITES_TABLE, self.access_level):
                    self.website_name_list.insert(0, web)
                    self.specific_or_general_list.insert(0, how_blocked)
                    self.master.blocked_websites.append((web, how_blocked,
                                                         self.access_level))
                    update_database(web, how_blocked, "add", self.access_level)
                else:
                    messagebox.showwarning("Already Blocked", f"{web} is already blocked")
            else:
                messagebox.showerror("Error", f"Can not add {web} to block.\n"
                                              "Please make sure you typed a valid name.")


def how_many_removed(how_remove, removed_count):
    """
    shows message of how many websites were removed from block
    :param how_remove: the way to remove from block
    :param removed_count: the number of websites that were removed
    :return:
    """
    if how_remove == GENERAL:
        messagebox.showinfo("Removed", f"{removed_count} websites were removed from block")
