# done ----------------------------------------

import _tkinter
import atexit
import datetime
import signal
import time
from tkinter import *
from tkinter import messagebox
from tkinter.font import Font
from socket import *
import sqlite3
import string
import os
import ctypes

from tkinter.ttk import Progressbar

import psutil
from getmac import get_mac_address
import json
import socket
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import binascii
import dns.resolver


APP_NAME = "Weblock"
WINDOW_HEIGHT = 700
WINDOW_WIDTH = 1000
SMALL_WINDOW_HEIGHT = 400
SMALL_WINDOW_WIDTH = 450
WINDOW_X_START = '400'
WINDOW_Y_START = '100'
POP_X_START = '500'
POP_Y_START = '200'
host_name = gethostname()
MY_IP = gethostbyname(host_name)
HOST = "127.0.0.1"
LOGIN_SERVER_IP = HOST # הכתובת של השרת , כתובה לאחר הכפתור addres
LOGIN_SERVER_PORT = 54321
PORT = 23456
GOOGLE_IP = "8.8.8.8"
MY_MAC = get_mac_address()
dns_resolver = dns.resolver.Resolver()
MY_DNS_SERVER = dns_resolver.nameservers[0]
BLOCKED_WEB_DB = sqlite3.connect('app.db', check_same_thread=False)
BLOCKED_WEB_CURSOR = BLOCKED_WEB_DB.cursor()
SPECIFIC = "Specific"
GENERAL = "General"
CLIENTS_TABLE = "clients connected"
DOT = "."
SPACE = " "
ENTER = "<Return>"
STARTING_ACCESS_LEVEL = '0'
MAC_EXISTS = "mac exists"
NAME_EXISTS = "name exists"
SUCCEED_MSG = 'ok'
NOT_SHOWN_WEBSITES_TABLE = 'not shown websites'
WEBSITES_TABLE = "websites blocked"
CLIENTS_STATS = "clients stats"
DISCONNECTED = "disconnected"
SIGN_UP = "sign up"
LOGIN = "login"
VALID = "valid"
INVALID = "invalid"
BUFFER_SIZE = 4
CHECKING_CONNECTION_MSG = ">>>???<<<"
ALREADY_CONNECTED = "already connected"
NOT_ALREADY_CONNECTED = "not already connected"
CONNECTION_FAILED = "&*)(#$%%*&$"
SERVER_CLOSED = "^*&%$#(^#&*$(^"
# characters that are allowed to be in a url
ALLOWED_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~:/?#[]@!$&'()*+,;=%"
ALLOWED_PUNCTUATIONS = "-._~:/?#[]@!$&'()*+,;=%"  # punctuations that are allowed to be in a url


def on_one_click(pressed_listbox, listboxes):
    """
    synchronizes the two listboxes to select the same item in the same time when user press on item
    :param pressed_listbox: the listbox the was pressed
    :param listboxes: all the listboxes on the screen
    :return: None
    """
    try:
        index_of_press = pressed_listbox.curselection()
        for listbox in listboxes:
            listbox.selection_clear(0, END)
            listbox.select_set(index_of_press)
    except _tkinter.TclError:
        pass


def confirm_clear(app, listboxes, table, name_or_access):
    """
    confirm with the user that he wants to clear the table
    :param app: the object of the App class
    :param listboxes: the listboxes on the screen
    :param table: the table of the database to clear from
    :param name_or_access: name of the client to clear his statistics or access level number to clear the websites
    :return: None
    """
    if table == CLIENTS_TABLE:
        text = "All clients will be removed.\n" \
               "Are you sure you wish to continue?"
    elif table == WEBSITES_TABLE:
        text = f"All websites will be removed from block in access level {name_or_access}.\n" \
               "Are you sure you wish to continue?"
    else:
        text = f"All website statistics of {name_or_access} will be removed.\n" \
               "Are you sure you wish to continue?"
    warning = messagebox.askquestion("Clear", text, icon="warning")
    if warning == "yes":
        clear_database(table, name_or_access)
        clear_app_list(app, table, name_or_access)
        clear_listboxes(listboxes, table)


def clear_app_list(app, table, access_level):
    """
    clears a list of the app according to what the manager chose to clear
    :param app: the object of the App class
    :param table: the table of the database to clear from
    :param access_level: the access level to clear websites from
    :return: None
    """
    if table == CLIENTS_TABLE:
        clients_names = [client for client in app.client_and_conns]  # gets all the clients names
        for client in clients_names:
            disconnect_client(app, client)  # disconnect the client
    elif table == WEBSITES_TABLE:
        # updates the blocked websites list to be without the blocked websites from the chosen access level
        app.blocked_websites = [blocked_web for blocked_web in app.blocked_websites if blocked_web[2] != access_level]


def clear_listboxes(listboxes, table):
    """
    clears the given listboxes
    :param listboxes: the listboxes to clear
    :param table: the table of the database to clear from
    :return: None
    """
    removed_count = listboxes[0].size()
    for listbox in listboxes:
        listbox.delete(0, END)
    if table != CLIENTS_STATS:
        removed_count_msg(table, removed_count)


def removed_count_msg(table, removed_count):
    if table == "clients connected":
        messagebox.showinfo("Removed Count", f"{removed_count} clients were removed")
    else:
        messagebox.showinfo("Removed Count", f"{removed_count} websites were removed")


def clear_database(table, name_or_access):
    """
    clears the database from all items
    :param table: the table of the database to clear from
    :param name_or_access: name of the client to clear his statistics or access level number to clear the websites
    :return: None
    """
    if table == WEBSITES_TABLE:
        clear_table_command = f"DELETE FROM '{table}' WHERE access_level='{name_or_access}';"
    elif table == CLIENTS_STATS:
        clear_table_command = f"DELETE FROM '{table}' WHERE client='{name_or_access}';"
    else:
        clear_table_command = f"DELETE FROM '{table}';"
    BLOCKED_WEB_CURSOR.execute(clear_table_command)
    BLOCKED_WEB_DB.commit()


def on_double_click(listboxes, index_of_press):
    """
    when the user press double click on a website in the list
     checking which item he pressed on and calling the popup func
    :param listboxes: the listboxes on the screen
    :param index_of_press: the index of the press in listbox
    :return: None
    """
    items = []
    for listbox in listboxes:
        items.append(listbox.get(index_of_press))
    return items


def bind_listboxes_scrollbar(listboxes, scrollbar):
    """
    binds all the given listboxes to the given scrollbar
    :param listboxes: the listboxes to bind together
    :param scrollbar: the scrollbar to bind the listboxes to
    :return: None
    """
    for listbox in listboxes:
        listbox.bind("<MouseWheel>", lambda e: on_mouse_wheel(e, listboxes))
        listbox.config(yscrollcommand=scrollbar.set)


def on_vsb(*args, listboxes):
    """
    When the scrollbar moves, scroll the listboxes
    :param args: the scroll number of the user
    :param listboxes: the listboxes to scroll
    :return: None
    """
    for listbox in listboxes:
        listbox.yview(*args)


def on_mouse_wheel(event, listboxes):
    """
    Convert mousewheel motion to scrollbar motion
    :param event: the mouse event
    :param listboxes: the listboxes to scroll
    :return: 'break' to prevent the default bindings from firing, which would end up scrolling the widget twice
    """
    for listbox in listboxes:
        listbox.yview_scroll(-1 * int(event.delta / 120), "units")
    return "break"


def insert_data_to_listboxes(table, listboxes, access_level):
    """
    insert data from database to the given listboxes
    :param table: the table in database to take data from
    :param listboxes: the listboxes to add data to
    :param access_level: the access_level to add data from
    :return: None
    """
    if table == WEBSITES_TABLE:
        exe_command = f"SELECT url, specific_or_general FROM '{table}' WHERE access_level='{access_level}'"
    else:
        exe_command = f"SELECT * FROM '{table}'"
    BLOCKED_WEB_CURSOR.execute(exe_command)
    for row in BLOCKED_WEB_CURSOR:
        for i in range(len(listboxes)):
            listboxes[i].insert(0, row[i])


def delete_item_by_index(listboxes, index):
    """
    deletes item from all listboxes in the given index
    :param listboxes: the listboxes to delete from
    :param index: the index of the item in listbox
    :return: None
    """
    for listbox in listboxes:
        listbox.delete(index)


def item_in_listbox(listbox, item):
    """
    checking if a given website is in the blocked list
    :param listbox: the listbox of the blocked websites
    :param item: the website to check
    :return: True if website in blocked list, False if not
    """
    websites_blocked = listbox.get(0, END)
    if item in websites_blocked:
        return True
    return False


def pop_exists(pop):
    """
    checks if the pop exists
    :param pop: the pop to check
    :return: True if exists, False if not
    """
    if pop is None:
        return False
    return True


def destroy_pop(pop):
    """
    destroys the pop if it exists
    :param pop: the pop to destroy
    :return: None
    """
    if pop_exists(pop):
        pop.destroy()


def is_valid(name):
    """
    checks if a given website to add doesn't have 2 punctuations in it
    :param name: the name of the website
    :return: True if valid, False if not
    """
    punctuations = string.punctuation
    if name in punctuations:
        return False
    for index in range(len(name) - 1):
        if (name[index] in ALLOWED_PUNCTUATIONS and name[index + 1] in ALLOWED_PUNCTUATIONS) \
                or name[index] not in ALLOWED_CHARACTERS:
            return False
    return True


def remove_none_string(lst):
    """
    remove all the string with nothing in it from list
    :param lst: the list to remove from
    :return: the new list without none strings
    """
    lst = [web for web in lst if web != ""]
    return lst


def check_client_exists_db(conn, name, mac):
    """
    checks if the given client name is already in the clients connected database
    :param conn: the socket connection with the client
    :param name: the name of the client
    :param mac: the mac of the client
    :return: True if exists, False if not
    """
    already_exists = check_mac_in_db(conn, mac)
    if not already_exists:
        already_exists = check_name_in_db(conn, name)
    return already_exists


def check_mac_in_db(conn, mac):
    """
    checks if the clients mac address is already exists
    :param conn: the socket connection with the client
    :param mac: the mac to check in database
    :return: True if the mac is in the clients database, False if not
    """
    exe_command = f"SELECT mac FROM '{CLIENTS_TABLE}' WHERE mac='{mac}'"
    BLOCKED_WEB_CURSOR.execute(exe_command)
    result = BLOCKED_WEB_CURSOR.fetchone()
    if result:  # if the mac exists in the database
        conn.send(MAC_EXISTS.encode())
        return True
    return False


def check_name_in_db(conn, name):
    """
    checks if the clients name is already exists
    :param conn: the socket connection with the client
    :param name: the name to check in database
    :return: True if the name is in the clients database, False if not
    """
    exe_command = f"SELECT name FROM '{CLIENTS_TABLE}' WHERE name='{name}'"
    BLOCKED_WEB_CURSOR.execute(exe_command)
    result = BLOCKED_WEB_CURSOR.fetchone()
    if result:  # if the name exists in the database
        conn.send(NAME_EXISTS.encode())
        return True
    return False


def web_in_db(web_name, table, access_level):
    """
    check if the website is in the datable
    :param web_name: the name of the website
    :param table: the table to check in
    :param access_level: the  access level of the website
    :return:
    """
    if table == WEBSITES_TABLE:
        exe_command = f"SELECT * FROM '{table}' WHERE url='{web_name}' AND access_level='{access_level}'"
    else:
        exe_command = f"SELECT * FROM '{table}' WHERE url='{web_name}'"
    BLOCKED_WEB_CURSOR.execute(exe_command)
    result = BLOCKED_WEB_CURSOR.fetchone()
    if result:
        return True
    return False


def update_database(web_name, specific_or_general, action, access_level):
    """
    updating the database according to the user action
    :param web_name: the name of the website
    :param specific_or_general: the the action is specific or general
    :param action: the action of the client (remove, add or change)
    :param access_level: the access level to add or remove from
    :return:
    """
    if action == "remove":
        remove_web_from_db(web_name, access_level, specific_or_general)
    elif action == "add":
        add_web_to_db(web_name, specific_or_general, access_level)
    else:
        change_web_db_status(web_name, specific_or_general, access_level)
    BLOCKED_WEB_DB.commit()


def remove_web_from_db(web_name, access_level, specific_or_general):
    """
    remove website from the database
    :param web_name: the name of the website
    :param access_level: the access level of the website
    :param specific_or_general: remove specific website or general (all websites that include the given website)
    :return: None
    """
    if specific_or_general == SPECIFIC:
        exe_command = f"DELETE FROM '{WEBSITES_TABLE}' WHERE url='{web_name}' AND access_level='{access_level}'"
    else:  # if the way to block is general
        exe_command = f"DELETE FROM '{WEBSITES_TABLE}' WHERE url LIKE '%{web_name}%' AND access_level='{access_level}'"
    BLOCKED_WEB_CURSOR.execute(exe_command)


def add_web_to_db(web_name, specific_or_general, access_level):
    """
    add website to the database
    :param web_name: the name of the website
    :param specific_or_general: the way of block to add the website as
    :param access_level: the access level to add to
    :return: None
    """
    add_command = f"INSERT INTO '{WEBSITES_TABLE}' (url, specific_or_general, access_level) VALUES (?, ?, ?)"
    values = (web_name, specific_or_general, access_level)
    BLOCKED_WEB_CURSOR.execute(add_command, values)


def change_web_db_status(web_name, specific_or_general, access_level):
    """
    change the status of how blocked to the given website
    :param web_name: the name of the website
    :param specific_or_general: way of block to update to
    :param access_level: the access level of the website
    :return: None
    """
    change_command = f"UPDATE '{WEBSITES_TABLE}' SET specific_or_general ='{specific_or_general}'" \
                     f" WHERE url='{web_name}' AND access_level='{access_level}' "
    BLOCKED_WEB_CURSOR.execute(change_command)


def del_future_shows(web_name, app):
    """
    adds the website to the db of the websites that their future in the client's stats shows are disabled
    :param web_name: the nae of the website
    :param app: the object of the App class
    :return:
    """
    # if the website is not already in the not shown websites
    if not web_in_db(web_name, NOT_SHOWN_WEBSITES_TABLE, None):
        BLOCKED_WEB_CURSOR.execute(f"INSERT INTO '{NOT_SHOWN_WEBSITES_TABLE}' VALUES ('{web_name}')")
        BLOCKED_WEB_DB.commit()  # add website to the database
        app.not_shown_websites.append(web_name)  # add the website to the not_shown_websites list in the app object
    else:
        messagebox.showerror("Error", f"Future shows of {web_name} already disabled")


def add_client_to_db(name, mac):
    """
    add the given client details to the database
    :param name: the name of the client
    :param mac: the mac address of the client
    :return: None
    """
    exe_command = f"INSERT INTO '{CLIENTS_TABLE}' (name,access_level,mac) VALUES (?, ?, ?)"
    values = (name, STARTING_ACCESS_LEVEL, mac)
    BLOCKED_WEB_CURSOR.execute(exe_command, values)
    BLOCKED_WEB_DB.commit()


def define_pop(frame, width, height):
    """
    define new popup
    :param frame: the frame that the pop is in
    :param width: the width of the pop
    :param height: the height of the pop
    :return: the pop
    """
    pop = Toplevel(frame)
    pop.geometry(f'{width}x{height}+{POP_X_START}+{POP_Y_START}')
    pop.resizable(False, False)
    pop.config()
    return pop


def close_socket(sock):
    """
    try to close the socket
    :param sock: the socket to close
    :return: None
    """
    try:
        sock.close()
    except Exception:
        pass


def get_name_from_mac(mac):
    """
    get name of the client from the mac
    :param mac: the mac address
    :return: the name that matches the given mac
    """
    exe_command = f"SELECT name FROM '{CLIENTS_TABLE}' WHERE mac='{mac}'"
    BLOCKED_WEB_CURSOR.execute(exe_command)
    result = BLOCKED_WEB_CURSOR.fetchone()
    if result:  # if there is a result
        name = result[0]
        return name


def check_already_connected(mac):
    """
    checks if the given mac is already connected to the main computer
    :param mac: the mac to get the name from
    :return: the name of the client. if the mac doesn't exits return None
    """
    name = get_name_from_mac(mac)
    if name:  # if the name for the given mac is not None
        return name
    else:
        return False


def define_app_size(app, width, height):
    """
    defines the size of the app
    :param app: the object of the App class
    :param width: the width of the screen
    :param height: the height of the screen
    :return: None
    """
    app.minsize(width, height)
    app.geometry(f"{width}x{height}")


def send_user_details(sock, action, name, password):
    """
    sends the given details to the admin server
    :param sock: the socket connection with the admin server
    :param action: sign up or login
    :param name: the name to send
    :param password: the password to send
    :return: None
    """
    encryptor = get_key(sock)  # get the encryptor to encrypt the password
    password = encryptor.encrypt(password.encode())  # encrypt the password with the public key encryptor
    # put all the details in a organized way with json
    msg = json.dumps({"action": action, "name": name, "password": binascii.hexlify(password).decode()})
    sock.send(msg.encode())  # send the details


def get_key(sock):
    """
    get the public key to encrypt the password
    :param sock: the socket connection with the admin server
    :return: the encryptor to encrypt the password
    """
    raw_key_numbers = sock.recv(1024).decode()  # get the public key numbers from the admin server
    key_numbers = json.loads(raw_key_numbers)  # load the key numbers to variable
    public_key = RSA.construct((key_numbers["n"], key_numbers["e"]))  # make the public key from the 2 numbers
    encryptor = PKCS1_OAEP.new(public_key)  # make an encryptor to encrypt the password
    return encryptor


def valid_password_form(password):
    """
    checks if the name or the password wasn't entered by the user. if one of the wasn't shows message of it
    :param password: the password to check
    :return: True if password valid, False if not
    """
    if password == "":
        messagebox.showwarning("", "Please enter a password")
        return False
    elif " " in password or not is_valid(password) or len(password) > 23:
        messagebox.showwarning("", "Invalid password format")
        return False
    return True


def check_valid_name(name):
    """
    check if the user gave a name and if its valid (doesn't have two punctuations in it)
    :param name: the name of the user
    :return: the username if it's valid, False if not
    """
    split_name = remove_none_string(name.split(" "))  # gets list of the name without spaces
    username = ''
    valid_name = True  # if the name is valid
    for w in split_name:
        if is_valid(w):  # if the part of the name is valid add it to the name
            username += w + " "
        else:  # if a part of the name is not valid (that means that all the name is not valid)
            valid_name = False
    if not valid_name:
        messagebox.showwarning("", "The given name is not valid.\n"
                                   "Please make sure it doesn't have two or more punctuations in a row.")
        return False
    elif username == '':  # if the name is none string
        messagebox.showwarning("", "Please enter a name")
        return False
    elif len(username) > 25:  # if the name is too long
        messagebox.showwarning("", "Name too long.\nPlease enter name with 1-25 characters")
        return False
    if username[-1] == " ":  # if the last letter of the name is space remove it
        return username[:-1:]
    return username


def valid_ip(ip):
    """
    checks if the user typed an ip address and if the ip is valid according to IPV4
    :param ip: the typed ip address
    :return: the ip address if valid, "" if not
    """
    ip = ip.replace(SPACE, DOT)
    ip_numbers = ip.split(DOT)
    ip_numbers = remove_none_string(ip_numbers)  # removes spaces from the ip
    valid = True  # if the ip is valid
    not_valid_text = "Please enter a valid address."  # the text that is shown if the ip is not valid
    for num in ip_numbers:  # moves over the numbers in the ip address
        # if a character is not a digit or a part of the ip has more than 3 characters
        if not num.isdigit() or len(num) > 3:
            valid = False
    if len(ip_numbers) == 0:  # if no address was entered
        messagebox.showwarning("", "Please enter a address")
    elif not valid:
        messagebox.showwarning("", not_valid_text)
    else:
        ip = ".".join(ip_numbers)
        if is_valid(ip) and has_3_dots(ip):  # if the ip is valid
            return ip
        else:
            messagebox.showwarning("", not_valid_text)
    return ""


def has_3_dots(ip):
    """
    checks if the ip address has 3 dots in it, because all ipv4 ip addresses needs to have 3 dots
    :param ip: the ip address to check
    :return: True if the ip address has 3 dots, false if not
    """
    count = 0
    for i in ip:
        if i == DOT:
            count += 1
    if count == 3 or count == 5:
        return True
    return False


def connect_to_server(server_address):
    """
    connecting to the DNS Proxy
    :param server_address: the address of the DNS Proxy
    :return: the socket connection with the DNS Proxy if the connection succeed
    """
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(3)
        sock.connect((server_address, PORT))
        print("connect succeed")
        return sock
    except Exception as e:
        pass


def check_response(response):
    """
    checks what is the server response to the connection. returns true if the response says the details are ok,
    otherwise returns false
    :param response: the response from the DNS Proxy
    :return: True if response ok, False if the response is mac exists or name exists
    """
    if response == MAC_EXISTS:
        messagebox.showerror("Error", "This computer is already connected to the main computer")
        return False
    elif response == NAME_EXISTS:
        messagebox.showwarning("Warning", "Name already exists.")
        return False
    return True


def delete_all(app):
    """
    delete all the widgets from the tkinter app
    :param app: the object of the App class
    :return: None
    """
    for obj in app.winfo_children():
        obj.destroy()


def disconnect_client(app, client_name):
    """
    sending to the client that he has been removed from the main computer
    :param app: the object of th App class
    :param client_name: the name of the client
    :return: None
    """
    try:
        conn = app.client_and_conns.pop(client_name)
        matched_len = match_buffer(str(len(DISCONNECTED)))
        conn.send(matched_len.encode())
        conn.send(DISCONNECTED.encode())
    except Exception:
        pass


def match_buffer(length):
    """
    make the length of the massage length match the buffer size of the socket
    :param length: the length of the massage
    :return: the length that matches the buffer
    """
    zeros_to_add = BUFFER_SIZE - len(length)
    matched_len = '0' * zeros_to_add + length
    return matched_len
