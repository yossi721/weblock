import requests
from scapy.all import *
from scapy.layers.dns import DNS, DNSQR, DNSRR, DNSRRSOA
import sqlite3
from scapy.layers.inet import IP, UDP, Ether
import dns.resolver
from urllib.error import URLError, HTTPError
from socket import *
import datetime
import socket
from threading import Thread
from block_website_flask import start_flask
from static_functions import match_buffer


BLOCKED_WEBS_TABLE = "blocked_websites"
CLIENT_MAC = "f4:6d:04:d4:a9:31"
host_name = gethostname()
MY_IP = gethostbyname(host_name)
DNS_PORT = 53
dns_resolver = dns.resolver.Resolver()
MY_DNS_SERVER = dns_resolver.nameservers[0]
BLOCKED_WEB_DB = sqlite3.connect('app.db', check_same_thread=False)
BLOCKED_WEB_CURSOR = BLOCKED_WEB_DB.cursor()
LOCAL_IP = "127.0.0.1"
CLIENTS_DB = sqlite3.connect('clients connected.db', check_same_thread=False)
CLIENTS_CURSOR = CLIENTS_DB.cursor()
CLIENTS_TABLE = "clients connected"
ACCESS_LEVEL_TABLE = "access level "
BLOCKED_STATUS = "  blocked"
NOT_BLOCKED_STATUS = "  not blocked"
NOT_SHOWN_WEBSITES_TABLE = 'not shown websites'
CLIENTS_STATS = "clients stats"
WEBSITES_TABLE = "websites blocked"
SPECIFIC = "Specific"
GENERAL = "General"
HTTPS = "https"
HTTP = "http"
BUFFER_SIZE = 4
TTL = 5

lock = threading.Lock()


def web_in_db(web_name, table):
    """
    checks if websites is in the given table
    :param web_name: the name of the website
    :param table: the table of the database to search in
    :return: True if the website is in the database, False if not
    """
    try:
        lock.acquire(True)
        exe_command = f"SELECT * FROM '{table}' WHERE url='{web_name}'"
        BLOCKED_WEB_CURSOR.execute(exe_command)
        result = BLOCKED_WEB_CURSOR.fetchone()
    finally:
        lock.release()
    if result:
        return True
    return False


def clients_connected():
    """
    selects the mac addresses and access levels in every raw of the clients connected table in the database
    :return: a list of ip addresses of all the clients connected to the dns proxy server
    """
    clients_mac_and_level = {}
    try:
        lock.acquire(True)
        exe_command = "SELECT mac, access_level FROM '" + CLIENTS_TABLE + "'"
        BLOCKED_WEB_CURSOR.execute(exe_command)
        for client in BLOCKED_WEB_CURSOR:
            mac_and_level = {client[0]: client[1]}  # client_mac_address is a tuple so i need the position 0 and 1 of it
            # to get the mac and the access level
            clients_mac_and_level.update(mac_and_level)
    finally:
        lock.release()
    return clients_mac_and_level


def get_name_from_mac(mac):
    """
    gets the name of the client from the mac address
    :param mac: the mac address
    :return: the name that matches the mac address
    """
    try:
        lock.acquire(True)
        exe_command = f"SELECT name FROM '{CLIENTS_TABLE}' WHERE mac='{mac}'"
        BLOCKED_WEB_CURSOR.execute(exe_command)
        result = BLOCKED_WEB_CURSOR.fetchone()
        name = result[0]
    finally:
        lock.release()
    return name


def make_http(web_name):
    """
    add http to the website url
    :param web_name: the name of the website
    :return: the url with http:// in it
    """
    if HTTP in web_name:
        return web_name
    else:
        url = "http://" + web_name  # the url of the website
        return url


def get_status_code(web_name):
    """
    return a status code of a website when you try reach it in the internet
    :param web_name: the name of the website
    :return: the status code
    """
    try:
        url = make_http(web_name)
        r = requests.get(url, allow_redirects=False, timeout=1.5)  # making get request to the website
        status_code = r.status_code  # the status code of the request
        socket.timeout
        return status_code
    except (HTTPError, URLError, TimeoutError, requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout):
        pass

def add_web_client_stats(name, web_name, status):
    """
    add website to the statistics of the client
    :param name: the name of the client
    :param web_name: the name of the website
    :param status: the status of the website (blocked or not blocked)
    """
    try:
        lock.acquire(True)
        exe_command = f"INSERT INTO '{CLIENTS_STATS}' (date, time, website, status, client) VALUES (?, ?, ?, ?, ?)"
        date_visited = datetime.datetime.today().strftime('%d/%m/%Y')  # the date of the dns request
        time_visited = datetime.datetime.now().strftime('%H:%M')  # the time of the dns request
        values = (date_visited, time_visited, web_name, status, name)
        BLOCKED_WEB_CURSOR.execute(exe_command, values)  # adds the values to the database
        BLOCKED_WEB_DB.commit()
    finally:
        lock.release()


def dns_filter(packet, app):
    """
    get only dns requests from the connected computers and if it is check the dns query
    :param packet: the received packet
    :param app: the object of the app
    :return: True if the packet is DNS query and the mac and the query is from client who is connected to the DNS proxy,
    False otherwise
    """
    clients_mac_and_level = clients_connected()  # gets the clients that are connected to the dns proxy
    # if the packet is a dns query and the computer is on of the connected computers
    if IP in packet and DNS in packet and DNSQR in packet and packet[Ether].src in clients_mac_and_level.keys():
        mac_address = packet[Ether].src
        access_level = clients_mac_and_level.get(mac_address)  # get the access level of the client
        name = get_name_from_mac(mac_address)  # gets the name of the client
        conn = app.client_and_conns.get(name)  # gets the connection with the client
        Thread(target=check_dns_query, args=(app, packet, access_level, name, conn), daemon=True).start()
        return True
    return False


def check_dns_query(app, dns_packet, access_level, name, conn):
    """
    moves over the blocked web databases and calls check_if_in_database with the name of the database
    :param app: the object of the App class
    :param dns_packet: the packet of the dns request
    :param access_level: the access level of the client
    :param name: the name of the client
    :param conn: the socket connection with the client
    :return: None
    """
    web_name = dns_packet[DNSQR].qname.decode()[:-1:]  # to index -1 because it adds a dot in the end of the url
    blocked = check_if_blocked(app, web_name, access_level)
    if blocked:
        status_code = get_status_code(web_name)
        # if the website is not http
        if status_code is None or status_code == 302 or status_code == 301 or status_code > 404:
            block(dns_packet, web_name, HTTPS, conn)
            add_web_client_stats(name, web_name, BLOCKED_STATUS)
        # if the website is http
        elif status_code < 404:
            block(dns_packet, web_name, HTTP, conn)
            add_web_client_stats(name, web_name, BLOCKED_STATUS)
    else:
        dns_response(dns_packet, web_name, name, app)


def check_if_blocked(app, web_name, access_level):
    """
    checks if the access level of the client is not 0. if it is 0 there is no need to check if blocked because none
    are blocked at access level 0.
    :param app: the object of the App class
    :param web_name: the name of the website the dns request was for
    :param access_level: the access level of the client
    :return: True if blocked, False if not
    """
    if access_level != '0':
        return check_blocked(app, web_name, access_level)
    return False


def check_blocked(app, web_name, access_level):
    """
    checks if the web is in the urls that are exactly like the manager wrote them
    returns true if the websites needs to be blocked, otherwise returns false
    :param app: the object of the App class
    :param web_name: the name of the website the dns request was for
    :param access_level: the access level of the client
    :return: True if blocked, False if not
    """
    for raw in app.blocked_websites:  # moves over the list of the blocked websites from the app
        # if the access level of the website is lower or equals to the access level of the client
        if int(raw[2]) <= int(access_level):
            # if the way to block is specific and the website that the client went to is the same as the blocked one
            if raw[1] == SPECIFIC and raw[0] == web_name:
                return True
            # if the way to block is general and if the the website that the client went has the blocked website
            # string in it
            elif raw[1] == GENERAL and raw[0] in web_name:
                return True
    return False


def block(dns_packet, web_name, protocol, conn):
    """
    checks if the website is http or https and blocks it in accordance
    :param dns_packet: the packet of the dns request
    :param web_name: the name of the website the dns request was for
    :param protocol: the protocol of the website (HTTP or HTTPS)
    :param conn: the socket connection with the client
    :return: None
    """
    if protocol == HTTPS:
        send_flask_ip(dns_packet)  # sending the dns response
        block_response_https(web_name, conn)  # sending to the client message that the website is blocked so he
        # can show message that the website is blocked
    else:
        send_flask_ip(dns_packet)  # sending the dns response


def block_response_https(web_name, conn):
    """
    sending to the client message that says the website is blocked
    :param web_name: the name of the website the dns request was for
    :param conn: the socket connection with the client
    :return: None
    """
    try:
        msg = f"You can not access {web_name}"
        print(msg)
        matched_len = match_buffer(str(len(msg)))
        conn.send(matched_len.encode())
        conn.send(msg.encode())
    except Exception as e:
        print(e)
        pass


def send_flask_ip(dns_packet):
    """
    blocks the website from loading by giving him dns response of my website that says that the website he went for
    is blocked
    :param dns_packet: the packet of the dns request
    :return: None
    """
    web_name = dns_packet[DNS].qd.qname  # the name of the website the surfer got into
    my_website_resp = IP(dst=dns_packet[IP].src) / UDP(dport=dns_packet[UDP].sport, sport=53) / \
                      DNS(id=dns_packet[DNS].id, ancount=1, qd=dns_packet[DNS].qd, aa=1, qr=1,
                          an=DNSRR(rrname=web_name, ttl=TTL, rdata=MY_IP))
    print("the response:")
    my_website_resp.show()
    send(my_website_resp, verbose=0)


def status_ok(status_code):
    """
    checks if the status code is ok which means the website exists
    :param status_code: the status code of the GET request to the website
    :return: True if status code is ok, False if not
    """
    if status_code is not None and status_code < 404:
        return True
    return False


def dns_response(dns_packet, web_name, name, app):
    """
    gives the client the requested dns response
    :param dns_packet: the packet of the dns request
    :param web_name: the name of the website the dns request was for
    :param name: the name of the client
    :param app: the object of the App class
    :return: None
    """
    try:
        status_code = get_status_code(web_name)  # gets the status code of the get request to the website
        response = sr1(IP(dst=MY_DNS_SERVER) /
                       UDP(sport=dns_packet[UDP].sport) /
                       DNS(rd=1, id=dns_packet[DNS].id, qd=DNSQR(qname=dns_packet[DNSQR].qname)),
                       verbose=0, timeout=2)  # sending the dns request to the dns server and get the response
        # create the response packet to the client
        resp_packet = IP(dst=dns_packet[IP].src, src=MY_IP) / UDP(dport=dns_packet[UDP].sport) / DNS()
        resp_packet[DNS] = response[DNS]  # copy the dns response from the dns server to the dns response for the client
        if status_ok(status_code):  # if the status code of the website is ok
            response[DNSRR].show()
            # changing the time to live that tells how much time the website is stored in the dns cache of the computer
            resp_packet[DNSRR].ttl = TTL
            resp_packet[DNSRR].show()
            # if the website is not one of the website that are not supposed to be shown in the statistics
            if web_name not in app.not_shown_websites:
                # add the website to the statistics of the client
                add_stat_thread = threading.Thread(target=add_web_client_stats,
                                                   args=(name, web_name, NOT_BLOCKED_STATUS))
                add_stat_thread.start()
        send(resp_packet, verbose=0)  # sending the dns response to the client
    except TypeError:
        pass


def sniff_dns(app):
    """
    sniffs the net for dns requests all the time
    :param app: the object of the App class
    :return: None
    """
    while True:
        sniff(count=1, lfilter=lambda x: dns_filter(x, app))


def start_dns_server(app):
    sniff_dns_thread = threading.Thread(target=sniff_dns, args=(app,), daemon=True)
    sniff_dns_thread.start()
    flask = threading.Thread(target=start_flask, daemon=True)
    flask.start()

