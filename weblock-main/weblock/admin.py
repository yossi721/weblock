import hashlib
from socket import *
import sqlite3
from threading import Thread
import socket
#import rsa
from getmac import get_mac_address
import pickle
import json
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import binascii
import os

host_name = gethostname()
HOST = "127.0.0.1"
IP = "0.0.0.0"
PORT = 54321
MY_MAC = get_mac_address()
USERS_DB = sqlite3.connect('users.db', check_same_thread=False)
USERS_CURSOR = USERS_DB.cursor()
USERS = "users"
SIGN_UP = "sign up"
LOGIN = "login"
VALID = "valid"
INVALID = "invalid"


def create_user_table():
    """
    the table where the user's username and password are saved
    :return: None
    """
    exe_command = f"CREATE TABLE IF NOT EXISTS '{USERS}'(\n" \
                  "username STRING,\n" \
                  "password STRING,\n" \
                  "salt STRING\n" \
                  ");"
    USERS_CURSOR.execute(exe_command)


def receive_details(client_socket, connected):
    """
    receives the name and password of the user and if he creates network or logging in
    :param client_socket: the socket with the client
    :param connected: True if the client is connected False if not
    :return: None
    """
    while connected:  # while the client is connected
        try:
            public_key, decryptor = create_rsa_keys()  # create the public and private key
            send_public_key(client_socket, public_key)  # send the public key to the client
            action, name, password = recv_details(client_socket)  # receive the details from the client
            password = decryptor.decrypt(binascii.unhexlify(password.encode()))  # decrypt the encrypted password
            if action == SIGN_UP:  # if the client is signing up
                add_to_db(name, password)  # add the details to the database
                client_socket.close()  # close the socket with the client
                connected = False  # stop the while loop
            else:  # if the client is logging in

                if not name_in_db(name) or not correct_password(name, password):  # if wrong name or password
                    client_socket.send(INVALID.encode())  # send invalid massage to the client
                else:  # if the name and the password are correct
                    client_socket.send(VALID.encode())  # send valid massage to the client
                    client_socket.close()  # close the socket with the client
                    connected = False  # stop the while loop
        except ConnectionResetError as e:  # if the connection has stopped (the client closed the socket)
            if e.errno == 10054:
                connected = False  # stop the while loop
        except Exception:
            pass


def send_public_key(client_socket, public_key):
    """
    sending the public key to the user
    :param client_socket: the socket connection with the client
    :param public_key: the public key for the RSA encryption
    :return: None
    """
    e = public_key.e  # the e number of the public key
    n = public_key.n  # the n number of the public key
    key_to_send = json.dumps({"e": e, "n": n})  # dumps the two public key numbers to be organized
    client_socket.send(key_to_send.encode())


def recv_details(client_socket):
    """
    receives the details from the user
    :param client_socket: the socket connection with the client
    :return: the details the client entered
    """
    raw_details = client_socket.recv(1024).decode()
    details = json.loads(raw_details)  # loads the details from the json
    return details["action"], details["name"], details["password"]


def add_to_db(name, password):
    """
    adding the details to the database
    :param name: the name of the user
    :param password: the password of the user
    :return:
    """
    hashed_pass, salt = generate_stored_values(password)
    exe_command = f"INSERT INTO {USERS} (username, password, salt) VALUES (?, ?, ?)"
    values = (name, hashed_pass, salt)
    USERS_CURSOR.execute(exe_command, values)
    USERS_DB.commit()


def generate_stored_values(password):
    """
    doing hash on the password with the salt and create the password to be stored in the database
    :param password: the password to hash
    :return: the value to be stored and the salt
    """
    salt = os.urandom(1024)  # creates random salt from 1024 bytes
    stored_pass = hashlib.pbkdf2_hmac('sha256', password, salt, 100000)  # doing hash between the password and the salt
    return stored_pass, salt


def name_in_db(name):
    """
    checks if the clients name is already exists
    :param name: the name to check
    :return: True if the name is in the database, False if not
    """
    exe_command = f"SELECT username FROM '{USERS}' WHERE username='{name}'"
    USERS_CURSOR.execute(exe_command)
    result = USERS_CURSOR.fetchall()
    if result:
        return True
    return False


def correct_password(name, password):
    """
    checks if the password the user entered for the current name is correct
    :param name: the name the user entered
    :param password: the password the user entered
    :return: True if the password is correct, False if not
    """
    exe_command = f"SELECT password, salt FROM '{USERS}' WHERE username='{name}'"
    USERS_CURSOR.execute(exe_command)
    rows = USERS_CURSOR.fetchall()
    # moves over all the users with the given username
    for row in rows:
        real_password = row[0]
        salt = row[1]
        hashed_pass = hashlib.pbkdf2_hmac('sha256', password, salt, 100000)  # doing hash between the password and
        # the salt
        if hashed_pass == real_password:  # if the passwords are the same
            return True
    return False


def create_rsa_keys():
    """
    creates private and public keys to the encryption
    :return: the public key and the decryptor to the public key
    """
    key_pair = RSA.generate(1024)  # generates RSA keys
    public_key = key_pair.public_key()  # the public key for the RSA
    decryptor = PKCS1_OAEP.new(key_pair)  # the decryptor that can decrypt the values that are encrypted by the
    # public key
    return public_key, decryptor


def main():
    create_user_table()  # creates the database table
    # opens socket
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((IP, PORT))
    server_socket.listen(2)
    while True:
        client_socket, address = server_socket.accept()  # accept new client
        connected = True
        Thread(target=receive_details, args=(client_socket, connected)).start()


if __name__ == '__main__':
    print("waiting...")
    main()






