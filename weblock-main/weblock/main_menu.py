# done ----------------------------------------

import _tkinter
from tkinter import *
from tkinter import messagebox
from tkinter.font import Font
from static_functions import *
from view_access_levels import ViewAccessLevels
from view_clients import ViewClientsConnected
from address import Address

HEADER_FONT = "Algerian 24 bold"
HEADER_COLOR = "dark slate gray"
HEADER_X_POS = 0.5
HEADER_Y_POS = 6
GREY = "#878C9F"
BLUE = "#4966C7"
FONT = "Helvetica 20"


class MainMenu(Frame):
    def __init__(self, master, last_frame):
        """
        the constructor function of the class
        :param master: the master of the class (the App class object)
        :param last_frame: the frame that was on the screen before the current frame
        """
        Frame.__init__(self, master)
        self.last_frame = last_frame
        master.protocol("WM_DELETE_WINDOW", lambda: x_pressed(master))
        self.header = Label(self, text="Main Menu", font=HEADER_FONT, fg=HEADER_COLOR)
        self.your_address_button = Button(self, text="Address", font=FONT, bg=GREY,
                                          command=lambda: master.switch_frame(self, Address))
        self.view_access_levels_button = Button(self, text="View Access Levels", font=FONT, bg="#92a8d1",
                                                command=lambda: master.switch_frame(self, ViewAccessLevels))
        self.view_users_connected_button = Button(self, text="View Users Connected", font=FONT, bg="#92a8d1",
                                                  command=lambda: master.switch_frame(self, ViewClientsConnected))
        self.place_widgets()

    def place_widgets(self):
        self.header.place(relx=HEADER_X_POS, y=HEADER_Y_POS, anchor='n')
        self.your_address_button.place(relx=0.02, rely=0.96, anchor='sw')
        self.view_access_levels_button.place(rely=0.34, relx=0.5, width=300, anchor=CENTER)
        self.view_users_connected_button.place(rely=0.51, relx=0.5, width=300, anchor=CENTER)


def x_pressed(app):
    """if the user pressed the exit button on the top right of the screen make sure he wants to exit"""
    warning = messagebox.askquestion("Exit", "Are you sure you wish to close the program?\n"
                                             "All computers connected will not have any connection to the internet.")
    if warning == "yes":
        print("warning is yes")
        app.destroy()
        sys.exit(-1)
